db.auth('admin', 'admin')
db = db.getSiblingDB('chatbot')
db.createUser(
    {
        user: "admin",
        pwd: "admin",
        roles: [
            {
                role: "dbOwner",
                db: "chatbot"
            }
        ]
    }
);