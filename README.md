# Project Title

Chatbot Accelerator Version 2.0

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

Note: The  implementation is tested on linux systems, it is recommended to use it on linux-based systems.  

### Setting up the Code  on Local System

#### Pre-requisites

1. MongoDB - https://docs.mongodb.com/getting-started/shell/installation/
	 - Navigate to Mongo installation folder, and in bin folder (E.g: C:\Program Files\MongoDB\Server\4.2\bin) execute mongo.exe.
     - In Mongo shell, run below commands
     ~~~
     use chatterbox; 
    db.createUser( 
   	{ 
     user: "chatterboxadmin", 
     pwd: "chatterboxadmin", 
     roles: [ "dbOwner" ] 
   	} 
	) 

     ~~~
2. ElasticSearch - https://www.elastic.co/downloads/elasticsearch
     - Run **elasticsearch.bat** file from '**elasticsearch-7.4.2/bin/'** folder of the Elastic search extract
3. Python 3.6.3 - https://www.python.org/downloads/release/python-363/
4. Node.Js - https://nodejs.org/en/download
    - After installing Node JS you can install Angular CLI by running following command in terminal / CMD    
    `npm install -g @angular/cli`
 
5. Clone the current repository to your local system

6. In windows machine one must have microsoft build tools(Build Tools for visual studio 2019) version 14.1 and above.
   Link- https://visualstudio.microsoft.com/downloads/#build-tools-for-visual-studio-2019

##### Setting dependencies for Backend code.
- Open Command prompt inside 'latam-mininons' folder and run the below commands to install required python packages
~~~
pip install -r admin/requirements.txt
pip install -r engine/requirements.txt
pip install -r socketio/setup/requirements.txt
python -m nltk.downloader "punkt"
python -m nltk.downloader "averaged_perceptron_tagger"
python -m nltk.downloader "stopwords"
~~~ 

- In case you face installation error during running engine code or socket code because of the package missing issue(Windows machine)
~~~
pip install --upgrade google-api-core
pip uninstall protobuf
pip install protobuf==3.6.0
~~~


##### Setting dependencies for Front-End
- Navigate to **'admin-ui/src'** folder and run the below command
~~~ 
npm install 
~~~
#### Starting the application (Recommended for Local Dev)
- Navigate to **'admin/src'** folder and run the below command
~~~
python main.py
~~~
    - The server will be hosted on port: 5000 on localhost
    - The Swagger docs are available on **http://localhost:5000/admin/api/docs**



- Navigate  to **'engine/'** folder and run the below command
~~~
python app.py
~~~

##### For Socket IO
- Navigate to **'socketio/'** folder and run the below command
~~~
python app.py
~~~
    - If you encounter this error: "socket.gaierror: [Errno 11001] No address found"
    - Set the APP_HOST environment variable to your current IP address: export APP_HOST=#.#.#.# or set APP_HOST=#.#.#.# (Windows)

##### For Admin-UI 
- route to **'admin-ui/src'** and run the below command
~~~
ng serve
~~~
- On successful compilation, the admin-ui will be hosted on port 4200 by default i.e, **http://localhost:4200/**


### Docker Deployment (Recommended for Prod): 

1. Clone the current repository on the local system
'''

'''
2. Open Command Prompt in 'latam-minions' folder. Build and Run the docker container using below command
```
docker-compose -p latamminions up -d 
```

## Ports exposed for the project

|                | Local Port | Docker Port |
|----------------|------------|-------------|
| MongoDB        | 27017      | 37017       |
| Elastic Search | 9200       | 39200       |
| Engine         | 8000       | 38004       |
| SocketIO       | 5003       | 37005       |
| Admin-backend  | 5000       | 35000       |
## Versioning

We use Bitbucket for versioning

## Authors

Pfizer IAS Engage Team

