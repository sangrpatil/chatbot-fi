import datetime
import time
import io
import bson
import pytz
from flask import Blueprint, request, jsonify, make_response, send_file
from flask import current_app as app
from flask_babel import refresh
from utility.execution_engine import ExecutionEngine
from utility.logger import logger
from utility.mongo_dao import get_by_id, get_one, update, insert
from utility.data import data
from utility.session_manager import get as get_context, set as set_context, set_chat_data, set_chat_stat_faq
from bson.objectid import ObjectId

bot_blueprint = Blueprint('bot', __name__, template_folder='templates')


@bot_blueprint.route('/check')
def check():
    return jsonify(
        status='Running'
    )

@bot_blueprint.route('/chatbotdata')
def chatbotdata():
    print([d for d in data['graph']])

    for collection in data:
        coll_data = data[collection]
        for d in coll_data:
            if d["_id"]:
                if not get_by_id(collection, d["_id"]):
                    insert(collection, d)

    return jsonify(
        status='Uploading chatbot data',
        data="data"
    )


class Language:
    lang = None

    def __init__(self, language):
        Language.lang = language

@bot_blueprint.route('/bot/<bot_id>', methods=['GET'])
def bot_details(bot_id):
    try:
        bot = get_by_id('bot_dto', bot_id)
        if bot:
            data = eval(str(bot))
            data["id"] = str(data.pop("_id"))
            return jsonify(status="Success", data=data), 200
        else:
            return jsonify(status='Failure', msg='Invalid Bot ID'), 404
    except Exception as e:
        return jsonify(status='Failure', msg='Invalid Bot ID'), 404


@bot_blueprint.route('/bot-avatar/<bot_id>', methods=['GET'])
def get_bot_avatar(bot_id):
    try:
        bot_id = str(bot_id)
        bot_avatar = get_one("avatar_dto", "bot_id", bot_id)
        if bot_avatar:
            data = eval(str(bot_avatar))
            data["id"] = str(data.pop("_id"))
            data["avatar"] = str(data.pop("avatar"))
            return jsonify(status="Success", data=data), 200
        else:
            return jsonify(status='Failure', msg='Invalid Bot ID'), 404
    except Exception as e:
        return jsonify(status='Failure', msg='Invalid Bot ID'), 404


@bot_blueprint.route('/web-console-theme/<theme_id>')
def get_web_console_theme(theme_id):
    try:
        web_console = get_by_id('ui_theme_dto', theme_id)
        if web_console:
            data = eval(str(web_console))
            data["id"] = str(data.pop("_id"))
            return jsonify(status="Success", data=data), 200
        else:
            return jsonify(status='Failure', msg='Invalid Web Console Theame ID'), 404
    except Exception as e:
        return jsonify(status='Failure', msg='Invalid Web Console Theame ID'), 404

@bot_blueprint.route('/bot/<bot_id>/image', methods=['GET'])
def bot_image(bot_id):
    image = get_one("avatar_dto", "bot_id", bot_id)
    if image:
        img_object = image.get("avatar")
        img_object = get_one("images.chunks", "files_id", img_object)["data"]
        in_memory_file = io.BytesIO(img_object)
        return send_file(in_memory_file, mimetype='image')
    else:
        return jsonify(message="Image not found"), 404

@bot_blueprint.route('/bot/<bot_id>/userimage', methods=['GET'])
def bot_user_image(bot_id):
    image = get_one("user_avatar_dto", "user_id", bot_id)
    if image:
        img_object = image.get("avatar")
        img_object = get_one("images.chunks", "files_id", img_object)["data"]
        in_memory_file = io.BytesIO(img_object)
        return send_file(in_memory_file, mimetype='image')
    else:
        return jsonify(message="Image not found"), 404

@bot_blueprint.route('/bot/<bot_id>/headerimage', methods=['GET'])
def bot_header_image(bot_id):
    image = get_one("header_avatar_dto", "user_id", bot_id)
    if image:
        img_object = image.get("avatar")
        img_object = get_one("images.chunks", "files_id", img_object)["data"]
        in_memory_file = io.BytesIO(img_object)
        return send_file(in_memory_file, mimetype='image')
    else:
        return jsonify(message="Image not found"), 404


# Chat Events
@bot_blueprint.route('/bot/<bot_id>/init', methods=['GET'])
def bot_init(bot_id):
    print("_____________________________________________")
    print("Session Created")
    print("_____________________________________________")
    logger.info('BOT ID: ', bot_id)
    try:
        bot = get_by_id('bot_dto', bot_id)
        if bot:
            if bot.get("chatflow") and get_by_id('chatflow_dto', bot.get("chatflow")):
                logger.debug('Valid bot id - %s', bot_id)
                session = dict()
                session['chatflow_id'] = bot.get('chatflow')
                session['on_connect_start'] = True
                session['suggestion'] = ''
                session['fuzzyFlag'] = False
                session['restartFlag'] = False
                session['restartFlagFAQ'] = False
                session['restartFlagNextTask'] = False
                session['queryFlag'] = False
                session['codeFlag'] = False
                session['guidedFlag'] = False
                session['audioSentFlag'] = False
                session['ASRFlag'] = False
                session['results'] = ''
                session['selectColumnQ'] = ''
                session['filterColumnQ'] = ''
                session['filterValueQ'] = ''
                session['surveyFlagNum'] = False
                session['surveyFlagComm'] = False
                session['endSurveyFlag'] = False
                session['feedbackFlag_ES'] = False
                session['suggestCounter_ES'] = 0
                session['audioSentFlag_ES'] = False
                session['bot_id'] = bot_id
                session['name'] = 'None'
                session['disable_input'] = False
                session['created_on'] = time.time()  # todo time zone
                session['subflow_type'] = ''
                session_id = set_context(session)
                chat_data = dict()
                chat_data["session_id"] = session_id
                chat_data["number_of_continuous_miss"] = 0
                chat_data["time"] = pytz.utc.localize(datetime.datetime.utcnow())
                chat_data["bot_id"] = bot_id
                chat_data["total_miss"] = 0
                chat_data["total_found"] = 0
                set_chat_data(chat_data)
                set_chat_stat_faq(chat_data)
                print("__________________________")
                print("Session _ID", session_id)
                print("__________________________")
                return jsonify(
                    status='Success',
                    session_id=session_id
                )
            else:
                return jsonify(
                    status='Failure',
                    msg='Invalid Chatflow'
                )
        else:
            return jsonify(
                status='Failure',
                msg='Invalid bot id'
            )
    except Exception as e:
        logger.error(str(e))
        return jsonify(
            status='Failure',
            msg='Invalid bot id'
        )


@bot_blueprint.route('/bot/<bot_id>/<session_id>/end', methods=['POST'])
def bot_end(bot_id, session_id):
    try:
        if bson.objectid.ObjectId.is_valid(bot_id) and get_by_id('bot_dto', bot_id):
            if bson.objectid.ObjectId.is_valid(session_id) and get_by_id('context', session_id):
                update('status', session_id, {'has_ended': True})
                return jsonify(
                    status='Success',
                    msg='Session ended'
                )
            else:
                return jsonify(
                    status='Failure',
                    msg='Not a valid Session ID'
                )
        else:
            return jsonify(
                status='Failure',
                msg='Not a valid Bot ID'
            )
    except Exception as e:
        logger.error("Bot error: ", str(e))


@bot_blueprint.route('/bot/<bot_id>/<session_id>/chat', methods=['POST'])
def bot_chat(bot_id, session_id):
    print("--------------------------------------------------------------")
    print("---------------------------------------------------------------")
    print("hit")
    print("---------------------------------------------------------------")
    print("---------------------------------------------------------------")
    session = get_context(session_id)
    bot_data = get_by_id("bot_dto", bot_id)
    if not session.get("chatflow_id"):
        session["chatflow_id"] = bot_data.get("chatflow")
        session["parent_chatflow"] = bot_data.get("chatflow")

    if not session.get("chatflow_id"):
        return jsonify(
            status='Failure',
            result='Invalid Chatflow id'
        )

    chatflow_id = session["chatflow_id"]
    chatflow_data = get_by_id("chatflow_dto", chatflow_id)
    mapping = chatflow_data.get("mapping")
    Language.lang = bot_data['bot_language']
    if (bson.objectid.ObjectId.is_valid(bot_id) and mapping):
        if (bson.objectid.ObjectId.is_valid(session_id) and get_by_id('context', session_id)):
            current_status = get_by_id('status', session_id)

            app.config['BABEL_DEFAULT_LOCALE'] = bot_data['bot_language']
            refresh()
            message_request = request.get_json(force=True)
            message = message_request['message']
            print("Hello USer message #################", message)

            # FI work-around
            if isinstance(message, list):
                # capture the selected option and return response

                filters_data = get_one("filters_dto", "session_id", session_id)
                filter_key = list(message[0].keys())[0]
                data = { filter_key: [ v[filter_key] for v in message ] }
                if not filters_data:
                    data["session_id"] = session_id
                    insert("filters_dto", data)
                else:
                    update("filters_dto", filters_data["_id"], data)
                #message = "Outstanding Balance"
                message = "Show Filters"
                message_request["message"] = message

            current_state = message_request['state']
            asr_flag = message_request['ASR']

            if session['on_connect_start']:
                execution_engine = ExecutionEngine(session_id, bot_id, chatflow_id)

                for state in mapping:
                    for key, value in state.items():
                        if key == 'is_start' and value:
                            response = execution_engine.execute(state)
                            return jsonify(
                                status='Success',
                                result=response,
                                current_state=get_by_id("status", session_id)["current_state"]
                            )
            else:
                # if condition - if msg is empty and on connet start is FALSE then just re-execute the current state
                # if we refresh, then just re-execute the current state.
                # pop out the last state in history because it will be added back in via conversation logger
                if message == '':
                    execution_engine = ExecutionEngine(session_id, bot_id, chatflow_id)
                    current_state = get_by_id("status", session_id)["current_state"]
                    state_definition = mapping
                    for state in state_definition:
                        for key, value in state.items():
                            if key == 'task_name' and value == current_state:
                                history = get_by_id('status', session_id)['history']
                                history = history[:-1]  # pop out the last element
                                update('status', session_id, {'history': history})
                                response = execution_engine.execute(state)
                                return jsonify(
                                    status='Success',
                                    result=response,
                                    current_state=get_by_id("status", session_id)["current_state"]
                                )
                else:
                    execution_engine = ExecutionEngine(session_id, bot_id, chatflow_id)
                    response = execution_engine.resume(message, asr_flag, bot_id)
                    print("################## bot response", response)
                    if response is None:
                        return jsonify(
                            status='Failure',
                            result=response,
                            current_state=get_by_id("status", session_id)["current_state"]
                        )
                    else:
                        return jsonify(
                            status='Success',
                            result=response,
                            current_state=get_by_id("status", session_id)["current_state"]
                        )
        else:
            return jsonify(
                status='Failure',
                result='Invalid session id'
            )
    else:
        return jsonify(
            status='Failure',
            result='Invalid bot id'
        )
