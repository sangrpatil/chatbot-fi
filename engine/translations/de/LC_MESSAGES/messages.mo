��    $      <              \  +   ]  
   �     �  #   �  ]   �     0     8  B   ;     ~      �     �      �  3   �  <   *  U   g  ?   �  ;   �  &   9  >   `  ?   �  >   �  !     (   @  ,   i  -   �      �     �  $   �  )        F  4   b  %   �     �     �     �  �  �  /   V	  
   �	     �	  #   �	  q   �	     A
     I
  6   N
  (   �
  #   �
     �
  -   �
  3      R   T  d   �  ?     X   L  A   �  >   �  e   &  >   �     �  -   �  ,     -   A  H   o     �  )   �  )   �  !      8   B  %   {     �     �     �   Are you interested in any of these options? Click Here Hit enter to send message I'm sorry Survey is not configured. I'm sorry none of those options were helpful. Would you like to try rephrasing your question? Message No On a scale of 1-5, how satisfied were you with your service today? Please choose an option 1-5. Please choose one of the options Please choose yes or no. Please leave your comment below: Please select any of the stores to view it on a map Sorry we don't have an answer for that. Could you try again? Sorry we werent able to find what you were looking for. Your feedback has been noted. Sorry we werent able to understand your intent. - EXECUTE Error Sorry, I cant find an answer for that. Could you try again? Sorry, there is nothing to go back to. Sorry, we do not have pharmacies within 50 miles of your area. Sorry, we may not have information for that. Can you try again? Sorry, we werent able to understand your intent - RESUME Error Thank you for your participation! Thank you! Your feedback has been added. Thanks for chatting, hope to talk again soon Thanks for chatting, hope to talk again soon! Thanks for using the Pfizer Bot! Was this helpful? We couldn't find that. Do you mean:  What other questions can I help you with? Would you like to continue? Would you like to continue? Please choose yes or no. Would you like to leave any comments? Yes no yes Project-Id-Version: PROJECT VERSION
Report-Msgid-Bugs-To: EMAIL@ADDRESS
POT-Creation-Date: 2019-08-28 20:28+0530
PO-Revision-Date: 2018-06-21 16:50-0500
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language: de
Language-Team: de <LL@li.org>
Plural-Forms: nplurals=2; plural=(n != 1)
MIME-Version: 1.0
Content-Type: text/plain; charset=utf-8
Content-Transfer-Encoding: 8bit
Generated-By: Babel 2.6.0
 Sind Sie an einer dieser Optionen interessiert? Klick hier Hit enter to send message I'm sorry Survey is not configured. Es tut mir leid, dass keine dieser Optionen hilfreich war. Möchten Sie versuchen, Ihre Frage neu zu formulieren? Message Nein Wie zufrieden waren Sie heute auf einer Skala von 1-5? Bitte wählen Sie eine der Optionen 1-5. Bitte wählen Sie eine der Optionen Bitte wählen Sie Ja oder Nein. Bitte hinterlassen Sie Ihren Kommentar unten: Please select any of the stores to view it on a map Entschuldigung, wir haben keine Antwort darauf. Könntest du es nochmal versuchen? Leider konnten wir nicht finden, wonach Sie gesucht haben. Ihr Feedback wurde zur Kenntnis genommen. Sorry we werent able to understand your intent. - EXECUTE Error Entschuldigung, ich kann keine Antwort dafür finden. Könntest du es nochmal versuchen? Entschuldigung, es gibt nichts, zu dem ich zurückkehren könnte. Sorry, we do not have pharmacies within 50 miles of your area. Entschuldigung, möglicherweise haben wir keine Informationen dafür. Kannst du es nochmal versuchen? Sorry, we werent able to understand your intent - RESUME Error Danke für Ihre Teilnahme! Vielen Dank! Ihr Feedback wurde hinzugefügt. Thanks for chatting, hope to talk again soon Thanks for chatting, hope to talk again soon! Vielen Dank, dass Sie den Chat ausprobiert haben. Bis zum nächsten Mal. War dies hilfreich? Das konnten wir nicht finden. Meinst du:  What other questions can I help you with? Haben Sie weitere Fragen an mich? Möchten Sie fortfahren? Bitte wählen Sie Ja oder Nein. Möchten Sie Kommentare hinterlassen? Ja nein ja 