# Portuguese (Brazil) translations for PROJECT.
# Copyright (C) 2019 ORGANIZATION
# This file is distributed under the same license as the PROJECT project.
# FIRST AUTHOR <EMAIL@ADDRESS>, 2019.
#
msgid ""
msgstr ""
"Project-Id-Version: PROJECT VERSION\n"
"Report-Msgid-Bugs-To: EMAIL@ADDRESS\n"
"POT-Creation-Date: 2019-08-28 20:28+0530\n"
"PO-Revision-Date: 2019-08-01 17:44+0530\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language: pt_BR\n"
"Language-Team: pt_BR <LL@li.org>\n"
"Plural-Forms: nplurals=2; plural=(n > 1)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Generated-By: Babel 2.6.0\n"

#: templates/chat.html:37
msgid "Message"
msgstr "mensagem"

#: templates/chat.html:45
msgid "Hit enter to send message"
msgstr "Pressione Enter para enviar mensagem"

#: utility/conversation_logger.py:126 utility/elasticsearch_query.py:356
#: utility/execution_engine.py:510 utility/execution_engine.py:562
#: utility/execution_engine.py:624 utility/execution_engine.py:660
#: utility/execution_engine.py:683
msgid "yes"
msgstr "sim"

#: utility/conversation_logger.py:126 utility/elasticsearch_query.py:371
#: utility/execution_engine.py:569 utility/execution_engine.py:646
#: utility/execution_engine.py:668 utility/execution_engine.py:696
msgid "no"
msgstr "não"

#: utility/conversation_logger.py:200 utility/execution_engine.py:549
msgid "We couldn't find that. Do you mean: "
msgstr "Nós não conseguimos encontrar isso. Você quer dizer:"

#: utility/conversation_logger.py:200 utility/execution_engine.py:554
msgid "Sorry we don't have an answer for that. Could you try again?"
msgstr "Desculpe, não temos uma resposta para isso. Você poderia tentar de novo?"

#: utility/elasticsearch_query.py:364
msgid "Thank you! Your feedback has been added."
msgstr "Obrigado! Seu feedback foi adicionado."

#: utility/elasticsearch_query.py:394 utility/elasticsearch_query.py:407
msgid ""
"Sorry we werent able to find what you were looking for. Your feedback has"
" been noted."
msgstr ""
"Desculpe, não conseguimos encontrar o que você estava procurando. Seu "
"feedback foi anotado."

#: utility/elasticsearch_query.py:432
msgid "Was this helpful?"
msgstr "Isto foi útil?"

#: utility/elasticsearch_query.py:433 utility/execution_engine.py:534
#: utility/execution_engine.py:549 utility/execution_engine.py:576
#: utility/execution_engine.py:655 utility/execution_engine.py:678
#: utility/execution_engine.py:707 utility/execution_engine.py:877
#: utility/execution_engine.py:888 utility/execution_engine.py:898
msgid "Yes"
msgstr "sim"

#: utility/elasticsearch_query.py:433 utility/execution_engine.py:534
#: utility/execution_engine.py:549 utility/execution_engine.py:576
#: utility/execution_engine.py:655 utility/execution_engine.py:678
#: utility/execution_engine.py:707 utility/execution_engine.py:877
#: utility/execution_engine.py:888 utility/execution_engine.py:898
msgid "No"
msgstr "não"

#: utility/elasticsearch_query.py:476 utility/elasticsearch_query.py:485
#: utility/elasticsearch_query.py:514
msgid "Sorry, I cant find an answer for that. Could you try again?"
msgstr "Sorry, I cant find an answer for that. Could you try again?"

#: utility/execution_engine.py:172
msgid "Sorry we werent able to understand your intent. - EXECUTE Error"
msgstr ""

#: utility/execution_engine.py:292
msgid "Sorry, we werent able to understand your intent - RESUME Error"
msgstr ""

#: utility/execution_engine.py:397
msgid "Click Here"
msgstr "Clique aqui"

#: utility/execution_engine.py:484
msgid "Sorry, there is nothing to go back to."
msgstr "Desculpe, não há nada para voltar."

#: utility/execution_engine.py:506
msgid "Please choose an option 1-5."
msgstr "Por favor escolha uma opção 1-5."

#: utility/execution_engine.py:511
msgid "Please leave your comment below:"
msgstr "Por favor, deixe o seu comentário abaixo:"

#: utility/execution_engine.py:518
msgid "Thank you for your participation!"
msgstr "Obrigado pela sua participação!"

#: utility/execution_engine.py:529
msgid "On a scale of 1-5, how satisfied were you with your service today?"
msgstr ""
"Em uma escala de 1 a 5, o quanto você ficou satisfeito com seu serviço "
"hoje?"

#: utility/execution_engine.py:534
msgid "Would you like to leave any comments?"
msgstr "Gostaria de deixar algum comentário?"

#: utility/execution_engine.py:576
msgid "Please choose yes or no."
msgstr "Por favor escolha sim ou não."

#: utility/execution_engine.py:650
msgid "Thanks for chatting, hope to talk again soon!"
msgstr "Obrigado por conversar, esperamos voltar a falar em breve!"

#: utility/execution_engine.py:655 utility/execution_engine.py:677
#: utility/execution_engine.py:706
msgid "Would you like to continue? Please choose yes or no."
msgstr "Você gostaria de continuar? Por favor escolha sim ou não."

#: utility/execution_engine.py:665
msgid "What other questions can I help you with?"
msgstr "Com que outras perguntas posso ajudá-lo?"

#: utility/execution_engine.py:672
msgid "Thanks for chatting, hope to talk again soon"
msgstr "Obrigado por conversar, esperamos conversar novamente em breve"

#: utility/execution_engine.py:700
msgid "Thanks for using the Pfizer Bot!"
msgstr "Obrigado por usar o Pfizer Bot!"

#: utility/execution_engine.py:782
msgid "Are you interested in any of these options?"
msgstr "Você está interessado em alguma dessas opções?"

#: utility/execution_engine.py:789
msgid "Sorry, we may not have information for that. Can you try again?"
msgstr "Desculpe, podemos não ter informações para isso. Você pode tentar de novo?"

#: utility/execution_engine.py:803
msgid ""
"I'm sorry none of those options were helpful. Would you like to try "
"rephrasing your question?"
msgstr ""
"Desculpe, nenhuma dessas opções foi útil. Você gostaria de tentar "
"reformular sua pergunta?"

#: utility/execution_engine.py:809
msgid "Please choose one of the options"
msgstr "Por favor, escolha uma das opções"

#: utility/execution_engine.py:853
msgid "Please select any of the stores to view it on a map"
msgstr "Por favor, selecione qualquer uma das lojas para visualizá-lo em um mapa"

#: utility/execution_engine.py:856
msgid "Sorry, we do not have pharmacies within 50 miles of your area."
msgstr "Desculpe, não temos farmácias dentro de 50 milhas da sua área."

#: utility/execution_engine.py:877 utility/execution_engine.py:887
#: utility/execution_engine.py:897
msgid "Would you like to continue?"
msgstr "Você gostaria de continuar?"

#: utility/execution_engine.py:907 utility/execution_engine.py:923
msgid "I'm sorry Survey is not configured."
msgstr "Desculpe, o Survey não está configurado."

#~ msgid "Sorry we werent able to understand your intent. - EXECUTE Error"
#~ msgstr ""
#~ "Desculpe, não fomos capazes de entender"
#~ " sua intenção. - Erro de execução"

#~ msgid "Sorry, we werent able to understand your intent - RESUME Error"
#~ msgstr "Não foi possível entender sua intenção - RESUME Error"

#~ msgid "Sorry, we are not able to understand your intent, EXECUTE Error"
#~ msgstr "Desculpe, não podemos entender sua intenção, executar um erro"

#~ msgid "Sorry, we are not able to understand your intent, RESUME Error"
#~ msgstr "Desculpe, não podemos entender sua intenção, retomar o erro"

