��    $      <              \  +   ]  
   �     �  #   �  ]   �     0     8  B   ;     ~      �     �      �  3   �  <   *  U   g  ?   �  ;   �  &   9  >   `  ?   �  >   �  !     (   @  ,   i  -   �      �     �  $   �  )        F  4   b  %   �     �     �     �  �  �  (   O	     x	  3   �	  #   �	  n   �	  	   \
     f
  P   m
  !   �
  (   �
  +   	  #   5  4   Y  G   �  r   �  ?   I  T   �  /   �  S     A   b  >   �  $   �  ;     J   D  L   �  5   �  !     B   4  @   w     �  F   �  !        <     @     G   Are you interested in any of these options? Click Here Hit enter to send message I'm sorry Survey is not configured. I'm sorry none of those options were helpful. Would you like to try rephrasing your question? Message No On a scale of 1-5, how satisfied were you with your service today? Please choose an option 1-5. Please choose one of the options Please choose yes or no. Please leave your comment below: Please select any of the stores to view it on a map Sorry we don't have an answer for that. Could you try again? Sorry we werent able to find what you were looking for. Your feedback has been noted. Sorry we werent able to understand your intent. - EXECUTE Error Sorry, I cant find an answer for that. Could you try again? Sorry, there is nothing to go back to. Sorry, we do not have pharmacies within 50 miles of your area. Sorry, we may not have information for that. Can you try again? Sorry, we werent able to understand your intent - RESUME Error Thank you for your participation! Thank you! Your feedback has been added. Thanks for chatting, hope to talk again soon Thanks for chatting, hope to talk again soon! Thanks for using the Pfizer Bot! Was this helpful? We couldn't find that. Do you mean:  What other questions can I help you with? Would you like to continue? Would you like to continue? Please choose yes or no. Would you like to leave any comments? Yes no yes Project-Id-Version: PROJECT VERSION
Report-Msgid-Bugs-To: EMAIL@ADDRESS
POT-Creation-Date: 2019-08-28 20:28+0530
PO-Revision-Date: 2018-08-10 09:30-0500
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language: ko
Language-Team: ko <LL@li.org>
Plural-Forms: nplurals=1; plural=0
MIME-Version: 1.0
Content-Type: text/plain; charset=utf-8
Content-Transfer-Encoding: 8bit
Generated-By: Babel 2.6.0
 이 옵션들에 관심이 있습니까? 여기를 클릭하십시오 메시지를 보내려면 Enter 키를 누릅니다 I'm sorry Survey is not configured. 그 옵션들 중 도움이되지 않아서 미안합니다. 질문에 대한 문구를 바꾸시겠습니까? 메시지 아니 1-5의 척도로 오늘 귀하의 봉사에 대해 얼마나 만족하십니까? 1-5 옵션을 선택하십시오. 옵션 중 하나를 선택하십시오. 예 또는 아니오를 선택하십시오. 아래에 의견을 남겨주세요 지도에서 보려는 상점을 선택하십시오. 죄송합니다. 답변이 없습니다. 다시해볼 수 있을까요? 죄송합니다. 귀하가 찾고자하는 것을 찾을 수 없었습니다. 귀하의 의견은 중요합니다. Sorry we werent able to understand your intent. - EXECUTE Error 미안 해요, 그 대답을 찾을 수 없습니다. 다시해볼 수 있을까요? 죄송합니다. 돌아갈 것이 없습니다. 죄송합니다. 귀하의 지역에서 50 마일 이내에 약국이 없습니다. 죄송합니다. 정보가 없습니다. 다시해볼 수 있니? Sorry, we werent able to understand your intent - RESUME Error 참여해 주셔서 감사합니다! 고맙습니다! 귀하의 의견이 추가되었습니다. 채팅 해 주셔서 감사 드리며 곧 다시 이야기하겠습니다. 채팅을 해주셔서 감사 드리며 곧 다시 이야기하겠습니다! 화이자 봇을 이용해 주셔서 감사합니다! 이게 도움이 되었습니까? 우리는 그것을 발견 할 수 없었다. 너는 의미하니? 제가 도와 드릴 수있는 다른 질문은 무엇입니까? 계속 하시겠습니까? 계속 하시겠습니까? 예 또는 아니오를 선택하십시오. 의견을 남기고 싶습니까? 예 아니 예 