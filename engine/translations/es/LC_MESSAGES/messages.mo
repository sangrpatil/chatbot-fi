��    $      <              \  +   ]  
   �     �  #   �  ]   �     0     8  B   ;     ~      �     �      �  3   �  <   *  U   g  ?   �  ;   �  &   9  >   `  ?   �  >   �  !     (   @  ,   i  -   �      �     �  $   �  )        F  4   b  %   �     �     �     �  �  �  0   V	     �	  %   �	  ,   �	  ^   �	     I
     Q
  G   T
  !   �
  #   �
     �
  -   �
  ;   +  M   g  Y   �  E     U   U  '   �  B   �  \     F   s      �  *   �  3     5   :  !   p     �  (   �  *   �     �  2     "   D     g     k     n   Are you interested in any of these options? Click Here Hit enter to send message I'm sorry Survey is not configured. I'm sorry none of those options were helpful. Would you like to try rephrasing your question? Message No On a scale of 1-5, how satisfied were you with your service today? Please choose an option 1-5. Please choose one of the options Please choose yes or no. Please leave your comment below: Please select any of the stores to view it on a map Sorry we don't have an answer for that. Could you try again? Sorry we werent able to find what you were looking for. Your feedback has been noted. Sorry we werent able to understand your intent. - EXECUTE Error Sorry, I cant find an answer for that. Could you try again? Sorry, there is nothing to go back to. Sorry, we do not have pharmacies within 50 miles of your area. Sorry, we may not have information for that. Can you try again? Sorry, we werent able to understand your intent - RESUME Error Thank you for your participation! Thank you! Your feedback has been added. Thanks for chatting, hope to talk again soon Thanks for chatting, hope to talk again soon! Thanks for using the Pfizer Bot! Was this helpful? We couldn't find that. Do you mean:  What other questions can I help you with? Would you like to continue? Would you like to continue? Please choose yes or no. Would you like to leave any comments? Yes no yes Project-Id-Version: PROJECT VERSION
Report-Msgid-Bugs-To: EMAIL@ADDRESS
POT-Creation-Date: 2019-08-28 20:28+0530
PO-Revision-Date: 2019-08-01 22:09+0530
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language: es
Language-Team: es <LL@li.org>
Plural-Forms: nplurals=2; plural=(n != 1)
MIME-Version: 1.0
Content-Type: text/plain; charset=utf-8
Content-Transfer-Encoding: 8bit
Generated-By: Babel 2.6.0
 ¿Estás interesado en alguna de estas opciones? Haga clic aquí Presione enter para enviar el mensaje Lo siento, la encuesta no está configurada. Lo siento, ninguna de esas opciones fue útil. ¿Te gustaría intentar reformular tu pregunta? Mensaje No En una escala de 1-5, ¿qué tan satisfecho estuvo con su servicio hoy? Por favor, elija una opción 1-5. Por favor elija una de las opciones Por favor elija sí o no.  Por favor deja tu comentario a continuación: Seleccione cualquiera de las tiendas para verlo en un mapa. Lo siento, no tenemos una respuesta para eso. ¿Podrías intentarlo de nuevo? Lo sentimos, no pudimos encontrar lo que estabas buscando. Su comentario ha sido anotado. Lo sentimos, no pudimos entender tu intención. - Error de ejecución Lo siento, no puedo encontrar una respuesta para eso. ¿Podrías intentarlo de nuevo? Lo siento, no hay nada a lo que volver. Lo sentimos, no tenemos farmacias dentro de 50 millas de su área. Lo sentimos, es posible que no tengamos información para eso. ¿Puedes intentarlo de nuevo? Lo sentimos, no pudimos entender tu intención - Error de REANUDACIÓN ¡Gracias por su participación! ¡Gracias! Su comentario ha sido agregado. Gracias por chatear, espero volver a hablar pronto. Gracias por chatear, ¡espero volver a hablar pronto! ¡Gracias por usar el Pfizer Bot! ¿Fue útil esto? No pudimos encontrar eso. Quieres decir: ¿Con qué otras preguntas puedo ayudarlo? ¿Te gustaria continuar? ¿Te gustaria continuar? Por favor elija sí o no. ¿Quieres dejar algún comentario? Sí no sí 