import os
import bson
import threading
import configparser
from flask_babel import Babel, gettext, refresh
from flask_cors import CORS
from flask import Flask, render_template, request, redirect, session, url_for, Response, jsonify, Markup

from routes.bot import bot_blueprint, Language

from utility.logger import logger
from utility.mongo_dao import get_collection
from utility.scheduler import notification_scheduler


app = Flask(__name__)
app.config['SECRET_KEY'] = 'D3l01TT3'
environment = os.getenv('FLASK_ENV', 'DEVELOPMENT').lower()
if environment == 'development':
    app.config['APPHOST'] = 'localhost'
elif environment == 'production':
    app.config['APPHOST'] = '0.0.0.0'


babel = Babel(app)

@babel.localeselector
def get_locale():
    refresh()
    if Language.lang == None:
        return "en"
    elif Language.lang == "esp":
        return "es"
    elif Language.lang == "por":
        return "pt"
    return "en"
CORS(app)
# Blueprints
app.register_blueprint(bot_blueprint)
notification_scheduler.start()

if __name__ == "__main__":
    app.run(host=app.config['APPHOST'], port=8000)

