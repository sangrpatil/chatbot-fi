import pytz
import bson
import datetime
from fuzzywuzzy import fuzz
from flask_babel import gettext
from utility.escalation_mail import send_email
from utility.mongo_dao import insert, update, get_by_id,get_all
from utility.session_manager import get as get_context, modify as update_context
from utility.logger import logger
from utility.escalate_data import EscalateData
from bson.objectid import ObjectId
from utility.scheduler import add_job_scheduler


class ConversationLogger():
    """
    Function: Log conversation from the bot and the user
    user_log logs user messages
    bot_log logs bot messages
    update is used by both user_log and bot_log to update MongoDB
    """

    def __init__(self, session_id, bot_id):
        self.session_id = session_id
        self.bot_id = bot_id
        self.ae_keywords = self.get_ae_keywords(bot_id) or []

        try:
            if not get_by_id('status', session_id):
                insert('status', {
                    "_id": bson.ObjectId(self.session_id),
                    "date_created": pytz.utc.localize(datetime.datetime.utcnow()),
                    "has_ended": False, "bot_id": bot_id
                })
                session = get_context(session_id)
                session['session_id'] = session_id
                session['session_exist'] = True
                update_context(session_id, session)
        except Exception as e:
            logger.error("Error during convo log Init:", str(e))

    def get_ae_keywords(self, bot_id):
        try:
            ae_keywords = []
            if not bot_id: return []

            ae_id = get_by_id('bot_dto', bot_id).get('ae_id')
            if not ae_id or not bson.objectid.ObjectId.is_valid(ae_id):
                return []

            ae_data = get_by_id('adverseevent_dto', ae_id)
            keywords = ae_data.get('keywords') or []

            for word in keywords:
                if word.get('keywords'):
                    ae_keywords.append(word.get('keywords'))
            return ae_keywords
        except Exception as e:
            return []

    def update(self, state):
        try:
            logger.debug('CURRENT SESSION - ', get_by_id('status', self.session_id)['history'],
                         type(get_by_id('status', self.session_id)['history']))
            current_state = get_by_id('status', self.session_id)['current_state']
            history = get_by_id('status', self.session_id)['history']
            logger.debug('STATE - ', state, ' HISTORY - ', history)
            history.append(state)
        except Exception as e:
            logger.exception('UPDATE EXCEPTION - ', e)
            current_state = ''
            history = [state]
        finally:
            data = {
                'current_state': state,
                'prior_state': current_state,
                'history': history,
                "date_created": pytz.utc.localize(datetime.datetime.utcnow())
            }
            update('status', self.session_id, {
                'current_state': state,
                'prior_state': current_state,
                'history': history,
                "date_created": pytz.utc.localize(datetime.datetime.utcnow())
            })

    def update_is_intent_found(self, message, is_intent_found):
        intent_type_mapper = {
            "intentFound": "INTENT FOUND",
            "intentNotFound": "INTENT NOT FOUND"
        }
        try:
            current_doc = get_by_id('status', self.session_id)
            current_index = len(current_doc.get(is_intent_found, []))
        except Exception as e:
            logger.exception('UPDATE {} EXCEPTION - '.format(intent_type_mapper.get(is_intent_found)), e)
            current_index = 0
        finally:
            type_message_index = is_intent_found + "." + str(current_index) + "." + is_intent_found
            time_index = is_intent_found + "." + str(current_index) + "." + "time"
            time = pytz.utc.localize(datetime.datetime.utcnow())
            update('status', self.session_id, {
                type_message_index: message,
                time_index: time
            })

    def updateIntentFoundCounter(self, message):
        self.update_is_intent_found(message, "intentFound")

    def updateIntentNotFound(self, message):
        self.update_is_intent_found(message, "intentNotFound")

    def user_log(self, user_msg, ASRFlag, endSurveyFlag, subflow_type, guidedRestartFlag):
        adverse_event = self.adverse_event_log(user_msg, subflow_type)
        current_doc = get_by_id('status', self.session_id)
        bot_id = current_doc['bot_id']
        date_created = current_doc['date_created']
        has_ended = current_doc['has_ended']
        current_state = current_doc['current_state']
        try:
            current_index = len(current_doc['conversations'])
            comments = rating = ""
            if str(endSurveyFlag) == 'True':
                if str(user_msg) in '12345':
                    rating = user_msg
                elif user_msg.lower().strip() != gettext('yes') and user_msg.lower().strip() != gettext('no'):
                    comments = user_msg
            error = False
        except Exception as e:
            logger.exception('Exception: ', str(e))
            current_index = 0
            comments = rating = ""
            error = True
        finally:
            type_message_index = "conversations." + str(current_index) + "." + "type"
            user_msg_index = "conversations." + str(current_index) + "." + "message"
            time_index = "conversations." + str(current_index) + "." + "time"
            fuzzy_index = "conversations." + str(current_index) + "." + "FuzzySearch"
            asr_index = "conversations." + str(current_index) + "." + "ASR"
            end_survey_index = "conversations." + str(current_index) + "." + "endSurveyFlag"
            time = pytz.utc.localize(datetime.datetime.utcnow())  # .strftime("%Y-%m-%d %H:%M")
            restartGuidedIndex = "conversations." + str(current_index) + "." + "restartGuidedIndex"

            update('status', self.session_id, {
                user_msg_index: user_msg,
                type_message_index: 'user message',
                time_index: time,
                fuzzy_index: error,
                asr_index: ASRFlag,
                end_survey_index: endSurveyFlag,
                restartGuidedIndex: guidedRestartFlag,
                'Comments': comments,
                'Rating': rating
            })

            subflow_type, subflow_name = self.get_subflow_data()
            insert('interactions', {
                "message": user_msg,
                "type": 'user message',
                "log_time": time,
                "FuzzySearch": error,
                "ASR": ASRFlag,
                "endSurveyFlag": endSurveyFlag,
                "restartGuidedIndex": guidedRestartFlag,
                "bot_id": bot_id,
                "date_created": date_created,
                "has_ended": has_ended,
                "current_state": current_state,
                "session_id": self.session_id,
                "Rating": rating,
                "Comments": comments,
                "adverse_event": adverse_event,
                "subflow_type": subflow_type,
                "subflow_name": subflow_name,
            })

    def bot_log(self, bot_msg, ASRFlag, endSurveyFlag, subflow_type, guidedRestartFlag):
        current_doc = get_by_id('status', self.session_id)
        bot_id = current_doc['bot_id']
        date_created = current_doc['date_created']
        has_ended = current_doc['has_ended']
        current_state = current_doc['current_state']
        try:
            current_count = len(current_doc['conversations'])
            current_index = current_count
        except Exception as e:
            logger.exception('Exception: ', str(e))
            current_index = 0
        finally:
            bot_msg_index = "conversations." + str(current_index) + "." + "message"
            type_message_index = "conversations." + str(current_index) + "." + "type"
            time_index = "conversations." + str(current_index) + "." + "time"
            time = pytz.utc.localize(datetime.datetime.utcnow())  # .strftime("%Y-%m-%d %H:%M")
            fuzzy_index = "conversations." + str(current_index) + "." + "FuzzySearch"
            asr_index = "conversations." + str(current_index) + "." + "ASR"
            end_survey_index = "conversations." + str(current_index) + "." + "endSurveyFlag"
            restartGuidedIndex = "conversations." + str(current_index) + "." + "restartGuidedIndex"
            current_chat_data = get_all("chat_data", "session_id", self.session_id)
            current_chat_data[0] = eval(current_chat_data[0])
            if gettext("We couldn\'t find that. Do you mean: ") in bot_msg or gettext(
                    "Sorry we don\'t have an answer for that. Could you try again?") in bot_msg:
                error = True
                current_chat_data[0]["number_of_continuous_miss"] += 1
                current_chat_data[0]["total_miss"] += 1
                update("chat_data", str(current_chat_data[0]["_id"]), current_chat_data[0])
                self.validate_and_escalate(bot_id, current_chat_data[0])
            else:
                error = False
                current_chat_data[0]["total_found"] += 1
                current_chat_data[0]["number_of_continuous_miss"] = 0
                update("chat_data", str(current_chat_data[0]["_id"]), current_chat_data[0])

            subflow_type, subflow_name = self.get_subflow_data()
            insert('interactions', {
                "message": bot_msg,
                "type": 'bot message',
                "log_time": time,
                "FuzzySearch": error,
                "ASR": ASRFlag,
                "endSurveyFlag": endSurveyFlag,
                "restartGuidedIndex": guidedRestartFlag,
                "bot_id": bot_id,
                "date_created": date_created,
                "has_ended": has_ended,
                "current_state": current_state,
                "session_id": self.session_id,
                "subflow_type": subflow_type,
                "subflow_name": subflow_name
            })

            update('status', self.session_id, {
                str(bot_msg_index): bot_msg,
                str(type_message_index): 'bot message',
                time_index: time,
                fuzzy_index: error,
                asr_index: ASRFlag,
                end_survey_index: endSurveyFlag,
                restartGuidedIndex: guidedRestartFlag
            })

    def adverse_event_log(self, message, subflow_type):
        """Log Adverse Event and return AE Keyword"""

        def insert_ae_log(msg, ae):
            # TODO - capture all the details
            return insert('adverse_event_log', {
                "creation": pytz.utc.localize(datetime.datetime.utcnow()),
                "chatflow_id": '',
                "subflow_id": '',
                "subflow_type": subflow_type,
                "bot_id": self.bot_id,
                "session_id": self.session_id,
                "message": msg,
                "message_type": '',
                "adverse_event": ae
            })

        try:
            msg = message.lower().strip()
            if not self.ae_keywords: return ''
            strg = ""
            word_list = []
            for word in self.ae_keywords:
                word_ = word.lower().strip()
                if word_ in msg:
                    ae_log = insert_ae_log(message, word_)
                    word_list.append(word_)

            strg = ",".join(word_list)
            return strg
        except Exception as e:
            return ''

    def validate_and_escalate(self, bot_id, dynamic_data):
        print(dynamic_data)
        try:
            print(bot_id, type(bot_id))
            bot = get_by_id("bot_dto", bot_id)
            escalation = None
            if bot:
                if ObjectId.is_valid(bot["esc_id"]):
                    escalation = get_by_id("escalation_dto", bot["esc_id"])
            print(escalation["escalation_trigger_number"])
            if escalation["esc_active_status"] == True and escalation["escalation_trigger_number"] <= dynamic_data["number_of_continuous_miss"]:
                output_report = EscalateData.create_report(self.session_id)
                if isinstance(output_report, str):
                    print("Failed to generate report log")
                    return False
                print("control is here")
                result = add_job_scheduler(output_report, escalation, send_email)
                print(result)
                #result = send_email(output_report, escalation)
                output_report.close()
                if result == "Success":
                    dynamic_data["number_of_continuous_miss"] = 0
                    update("chat_data", dynamic_data["_id"], dynamic_data)
                    print("escalation successful")
                else:
                    print("Received:", result)
                    logger.error(result)
                return True
            else:
                print("No escalation")
                return False
        except Exception as e:
            logger.error(str(e))
            return False

    def get_subflow_data(self):
        session = get_context(self.session_id)
        subflow_name = ""
        subflow_type = session.get("subflow_type", "")
        subflow_id = session.get("subflow_id")
        if subflow_type and subflow_id:
            if subflow_type.lower().strip() == "faq":
                subflow_name = get_by_id('faq_dto', subflow_id).get("name")
            else:
                subflow_name = get_by_id('chatflow_dto', subflow_id).get("name")

        return subflow_type, subflow_name