import configparser
import datetime
import itertools
import json
import math
import os
import re
import string

import nltk
import pytz
from bson.objectid import ObjectId
from elasticsearch import Elasticsearch
from flask_babel import gettext
from flask_socketio import emit
from fuzzywuzzy import fuzz
from utility.conversation_logger import ConversationLogger
from utility.escalate_data import EscalateData
from utility.escalation_mail import send_email
from utility.logger import logger
from utility.mongo_dao import get_by_id
from utility.mongo_dao import update, get_all, get_one, insert
from utility.scheduler import add_job_scheduler
from utility.session_manager import get as get_context, modify as update_context
from utility.svm_algorithm import SVMAlgorithm
from utility.tts import synthesize_text

# Global variables

app_config = configparser.ConfigParser()
app_config.read_file(open(r'config/app_settings.ini'))

elasticsearch_host = os.environ.get('ES_HOST') or 'localhost'
elasticsearch_port = os.environ.get('ES_PORT') or '9200'
elasticsearch_threshold = os.environ.get('ES_THRESHOLD') or '2.0'


class SearchES:

    def __init__(self, session_id, bot_id, faq_id):
        self.session_id = session_id
        self.session = get_context(session_id)
        self.conv_log = ConversationLogger(session_id, bot_id)
        self.faq_id = faq_id

    def build_response(self, type, static_text, interaction, interaction_elements, is_multi_message=False,
                       existing_json=None):
        if existing_json is None:
            existing_json = {}
        try:
            # sending a message - (multimessage=True, [type,static_text,interaction,interaction_elements])
            self.conv_log.bot_log(static_text, self.session['ASRFlag'], self.session['endSurveyFlag'], "FAQ",
                                  guidedRestartFlag=False)
            if self.session['ASRFlag']:
                static_text1 = re.sub(r'<[^<]+?>', ' ', static_text)
                tts_text = synthesize_text(static_text1)
                asr_use = True
                self.session['ASRFlag'] = False
                update_context(self.session_id, self.session)
            else:
                tts_text = ""
                asr_use = False
            if is_multi_message:
                existing_json['message'].append({
                    "interaction elements": interaction_elements,
                    "text": static_text,
                    "type": type,
                    "interaction": interaction,
                    "tts": asr_use,
                    "tts audio": tts_text,
                    "disable_response": self.session['disable_input']
                })
                existing_json['is_multi'] = True
                return existing_json
            else:
                json_return = {
                    "is_multi": False,
                    "message": [{
                        "interaction elements": interaction_elements,
                        "text": static_text,
                        "type": type,
                        "interaction": interaction,
                        "tts": asr_use,
                        "tts audio": tts_text,
                        "disable_response": self.session['disable_input']
                    }]
                }
                return json_return
        except Exception as e:
            logger.error("Elastic search Build response error: ", str(e))

    # based off feedback, update weight of elasticsearch answers
    @staticmethod
    def update_votes(score_answer, faq_id):
        try:
            print("Score Answer: ", score_answer)
            updated_score_answer = score_answer
            new_source = json.loads(json.dumps(updated_score_answer[2]))

            l = {"doc": {"qid": updated_score_answer[1], "question_set": new_source['question_set'],
                         "answer": updated_score_answer[3],
                         "votes": int(new_source['votes']) + 1}}
            # new_source["votes"] = int(new_source['votes']) + 1
            sample_data = {

                "doc": {
                    "id": updated_score_answer[1],
                    "answer": new_source['answer'],
                    "question_set": new_source['question_set'],
                    "votes": new_source['votes'] + 1
                }
            }
            es = Elasticsearch([{'host': str(elasticsearch_host), 'port': int(elasticsearch_port)}])

            status = es.update(index=str(faq_id), doc_type=str(faq_id), id=score_answer[1], body=sample_data)
        except Exception as e:
            logger.error("Elastic search Build response error: ", str(e))

    @staticmethod
    def score_modifier(question, question_from_es, keyphrases_from_es, keyphrases_from_question):
        """helper function to adjust score to return relevant answer"""
        global elasticsearch_threshold
        gradient = 0
        if fuzz.ratio(question, question_from_es) > 75:
            print("question value added")
            gradient += 0.0667 * elasticsearch_threshold
        else:
            print("question value subtracted")
            gradient -= 0.0667 * elasticsearch_threshold
        for i in range(0, len(keyphrases_from_question)):
            for j in range(0, len(keyphrases_from_es)):
                if fuzz.ratio(keyphrases_from_question[i], question_from_es[j]) > 66 \
                        or keyphrases_from_question[i].lower().strip() in keyphrases_from_es[j].lower().strip() \
                        or keyphrases_from_es[j].lower().strip() in keyphrases_from_question[i].lower().strip():
                    print("keywords value added")
                    print(keyphrases_from_question[i], )
                    gradient += 0.1667 * elasticsearch_threshold
                else:
                    print("keywords value subtracted")
                    gradient -= 0.2333 * elasticsearch_threshold
        return gradient

    # search for answer within english elasticsearch
    def search_es(self, question, faq_id):
        try:
            es = Elasticsearch([{'host': str(elasticsearch_host), 'port': int(elasticsearch_port)}])

            match_query = [
                {
                    "multi_match": {
                        "fuzziness": "AUTO",
                        "query": str(question).lower().strip(),
                        "fields": ["question_set.question", "question_set.key_phrases"],
                        "analyzer": "es_analyzer"
                    }
                }
            ]

            # add keywords query
            keywords_extracted = self.extract_key_phrases(question)
            keywords_list = [str(element) for element in keywords_extracted]
            keywords_string = " OR ".join(keywords_list)
            if keywords_string.strip():
                keyword_query = {
                    "multi_match": {
                        "query": keywords_string,
                        "fields": ["question_set.key_phrases"],
                        "analyzer": "es_analyzer",
                        "fuzziness": "AUTO"
                    }
                }
                match_query.append(keyword_query)

            # add question type query
            question_type = self.predict_question_type(question)
            if question_type:
                question_type_query = {
                    "multi_match": {
                        "query": question_type,
                        "fields": ["question_set.question_type"],
                        "analyzer": "es_analyzer",
                        "fuzziness": "AUTO"
                    }
                }
                match_query.append(question_type_query)

            data = {
                "query": {
                    "function_score": {
                        "query": {
                            "bool": {
                                "should": match_query
                            }},
                        "field_value_factor": {
                            "field": "votes",
                            "modifier": "log2p",
                            "factor": 2
                        },
                        "boost_mode": "sum"
                    }
                }
            }

            response = es.search(index=str(faq_id), body=data)
            score_answer = self.get_score_answer(question, keywords_list, response)
            if not score_answer or not len(score_answer):
                # best match query
                best_match_query = {
                    "query": {
                        "function_score": {
                            "query": {
                                "multi_match": {
                                    "query": str(question).lower().strip(),
                                    "fields": ["question_set.question", "question_set.key_phrases"],
                                    "type": "best_fields",
                                    "analyzer": "es_analyzer"
                                }
                            },
                            "boost": "2",
                            "boost_mode": "sum"
                        }
                    }
                }
                response = es.search(index=str(faq_id), body=best_match_query)
                score_answer = self.get_score_answer(question, keywords_list, response)
            return score_answer
        except Exception as e:
            logger.error("Elastic search searches error: ", str(e))

    def get_score_answer(self, question, keywords_list, response):
        """return best answer as per threshhold in config"""
        try:
            score = answer = source = id = ''
            score_answer = []
            answer_list = []
            if isinstance(response, (list, tuple)):
                response.sort(key="_score", reverse=True)
            for items in response['hits']['hits']:
                score = items['_score']
                source = items['_source']
                id = items['_id']
                answer = source['answer']
                votes = source['votes']
                print("Votes: ", votes)
                score += self.score_modifier(question, source["question_set"][0]["question"],
                    source["question_set"][0]["key_phrases"], keywords_list)
                # print("Original Score Without factor", str((score) / (math.log10(votes + 2))))
                # TODO - modify the logic
                print("modified score", score, elasticsearch_threshold)
                if score < float(elasticsearch_threshold):
                    print('This answer score is below threshold - Skipping this answer = {}'.format(answer))
                    continue
                else:
                    if answer not in answer_list:
                        answer_list.append(answer)
                        print('APPENDING THIS ANSWER = {}'.format(answer))
                        score_answer.append([score, id, source, answer, votes])
            return score_answer
        except Exception as e:
            logger.error("failed to get score answer {}".format(e))
            return []

    # search for answers within Spanish elasticsearch
    def search_es_non_english(self, question, faq_id, elasticsearch_analyzer):

        es = Elasticsearch([{'host': str(elasticsearch_host), 'port': int(elasticsearch_port)}])
        data = {
            "query": {
                "function_score": {

                    "query": {

                        "multi_match": {
                            "type": "phrase",
                            "query": question,
                            "fields": ["question_set.question"]

                        }
                    },

                    "field_value_factor": {
                        "field": "votes",
                        "modifier": "log2p"
                    }

                }
            }
        }
        response = es.search(index=str(faq_id), body=data)
        score = ''
        answer = ''
        source = ''
        id = ''
        print('RESPONSE FROM ELASTICSEARCH = {}'.format(response))
        exact_match = True
        if response['hits']['total'] == 0:
            exact_match = False
            print('No Exact match found. Going to execute Best Match')
            data = {
                "query": {
                    "function_score": {

                        "query": {

                            "multi_match": {
                                "type": "best_fields",
                                "query": question,
                                "fields": ["question_set.question"]

                            }
                        },

                        "field_value_factor": {
                            "field": "votes",
                            "modifier": "log2p"
                        }

                    }
                }
            }
            response = es.search(index=str(faq_id), body=data)
            print('RESPONSE FROM ELASTICSEARCH AFTER BEST FIELDS MATCH= {}'.format(response))

        # score_answer is - [score,id,source,answer]
        score_answer = []
        for items in response['hits']['hits']:
            score = items['_score']
            source = items['_source']
            id = items['_id']
            answer = source['a']
            votes = source['votes']
            original_score = ((score) / (math.log10(votes + 2)))
            if exact_match == False:
                if ((score) / (math.log10(votes + 1))) < float(elasticsearch_threshold):
                    continue
                else:
                    score_answer.append([score, id, source, answer, votes])
            else:
                score_answer.append([score, id, source, answer, votes])

        return (score_answer)

    def fetch_threshhold(self, faq_id, default_value):
        global elasticsearch_threshold
        es_analyzer = "en"

        faq_config = get_one("faq_config_dto", "faq_id", faq_id)
        faq_data = get_by_id("faq_dto", faq_id)
        if faq_config and faq_data:
            try:
                threshold = float(faq_config.get("threshold"))
                es_analyzer = faq_data.get("language")
                elasticsearch_threshold = threshold
                return int(float(faq_config.get("number_of_retries"))), es_analyzer
            except Exception as e:
                logger.error(e)
                elasticsearch_threshold = "3.0"
        else:
            es_analyzer = "en"
            return default_value, es_analyzer

    # take users message and determine what to do with it
    # will either be feedback yes/no or will be a message to query
    def execute(self, message, BotName, faq_id, ASR):  # message,bot_id, self.session['ASRFlag'],es_analyzer
        # TODO - make es_analyzer dynamic
        if message == "Go Back":
            return 2, ''
        faq_config = self.fetch_threshhold(faq_id, 5)
        es_analyzer = faq_config[1] if faq_config else "en"

        current_chat_data = get_all("chat_stat_faq", "session_id", self.session_id)
        current_chat_data[0] = eval(current_chat_data[0])
        bot_id = get_context(self.session_id)["bot_id"]
        if self.session['feedbackFlag_ES'] == True:
            if message.lower().strip() == gettext("yes"):
                current_chat_data[0]["total_found"] += 1
                current_chat_data[0]["number_of_continuous_miss"] = 0
                update("chat_stat_faq", str(current_chat_data[0]["_id"]), current_chat_data[0])
                updated_score_answer = self.session['es_reponse'][0]

                self.update_votes(updated_score_answer, faq_id)

                response = self.build_response("string", gettext('Thank you! Your feedback has been added.'), "text",
                                               "")
                self.session['suggestCounter_ES'] = 0
                self.session['feedbackFlag_ES'] = False
                update_context(self.session_id, self.session)
                self.intentFoundRecord(self.session['currentQuestion'])
                return 0, response
            elif message.lower().strip() == gettext('no'):
                self.session['suggestCounter_ES'] += 1
                update_context(self.session_id, self.session)
                current_chat_data[0]["number_of_continuous_miss"] += 1
                current_chat_data[0]["total_miss"] += 1
                update("chat_stat_faq", str(current_chat_data[0]["_id"]), current_chat_data[0])
                self.validate_and_escalate(bot_id, current_chat_data[0])
                if self.session['suggestCounter_ES'] < self.fetch_threshhold(faq_id, 5)[0]:
                    try:
                        if self.session['es_reponse']:
                            del self.session['es_reponse'][0]
                            response = self.build_response("string", self.session['es_reponse'][0][3], "text", "")
                            response_json_2 = self.feedback(response)
                            self.session['feedbackFlag_ES'] = True
                            update_context(self.session_id, self.session)
                            return 1, response_json_2
                            # return code,message
                    except Exception as e:
                        logger.error("Elasticsearch Exception error: ", str(e))
                        self.session['feedbackFlag_ES'] = False
                        self.intentNotFoundRecord(self.session['currentQuestion'])
                        self.session['suggestCounter_ES'] = 0
                        update_context(self.session_id, self.session)
                        response_json = self.build_response("string", gettext(
                            'Sorry we weren''t able to find what you were looking for. Your feedback has been noted.'),
                                                            "text", "")
                        return 0, response_json
                else:
                    self.intentNotFoundRecord(self.session['currentQuestion'])
                    self.session['suggestCounter_ES'] = 0
                    self.session['feedbackFlag_ES'] = False
                    update_context(self.session_id, self.session)
                    current_chat_data[0]["number_of_continuous_miss"] += 1
                    current_chat_data[0]["total_miss"] += 1
                    update("chat_stat_faq", str(current_chat_data[0]["_id"]), current_chat_data[0])
                    self.validate_and_escalate(bot_id, current_chat_data[0])
                    response_json = self.build_response("string", gettext(
                        'Sorry we weren''t able to find what you were looking for. Your feedback has been noted.'),
                                                        "text", "")
                    return 3, response_json
            else:
                self.session['currentQuestion'] = message
                self.session['feedbackFlag_ES'] = False
                update_context(self.session_id, self.session)
                response_json = self.checkResponse(message, faq_id, es_analyzer)
                return 1, response_json
        else:
            self.session['currentQuestion'] = message
            update_context(self.session_id, self.session)
            response_query = self.checkResponse(message, faq_id, es_analyzer)
            return 1, response_query

    # send message and send TTS audio
    def send_msg(self, msg):
        # global ASRFlag
        endSurveyFlag = False
        self.conv_log.bot_log(msg, self.session['ASRFlag'], endSurveyFlag, "FAQ")
        emit('message', msg)

    # check feedback, whether or not user was satisfied with the answer returned by elasticsearch
    def feedback(self, input_json):
        json = self.build_response("list", gettext('Was this helpful?'), "button_horizontal",
                                   [gettext('Yes'), gettext('No')], True, input_json)
        self.session['feedbackFlag_ES'] = True
        update_context(self.session_id, self.session)
        return json

    # if the user is actually
    def checkResponse(self, message, faq_id, es_analyzer):
        self.session['es_reponse'] = self.search_es(message, faq_id)
        self.session['es_reponse'].sort(key=lambda x: int(x[0]), reverse=True)
        print("length of response:  ", len(self.session['es_reponse']))
        current_chat_data = get_all("chat_stat_faq", "session_id", self.session_id)
        current_chat_data[0] = eval(current_chat_data[0])
        if len(self.session['es_reponse']) > 0:
            response_json = self.build_response("string", self.session['es_reponse'][0][3], "text", "")
            response_json_2 = self.feedback(response_json)

            self.session['feedbackFlag_ES'] = True
            update_context(self.session_id, self.session)
            return response_json_2

        else:
            # search keywords
            # todo - manage this in main query itself
            keywords = self.extract_key_phrases(message)
            if len(keywords):
                message = " ".join([msg.lower().strip() for msg in keywords])
                self.session['es_reponse'] = self.search_es(message, faq_id)
                self.session['es_reponse'].sort(key=lambda x: int(x[0]), reverse=True)
                if len(self.session['es_reponse']) > 0:
                    response_json = self.build_response("string", self.session['es_reponse'][0][3], "text", "")
                    response_json_2 = self.feedback(response_json)

                    self.session['feedbackFlag_ES'] = True
                    update_context(self.session_id, self.session)
                    current_chat_data[0]["total_found"] += 1
                    current_chat_data[0]["number_of_continuous_miss"] = 0
                    update("chat_stat_faq", str(current_chat_data[0]["_id"]), current_chat_data[0])
                    return response_json_2
                else:
                    current_chat_data[0]["number_of_continuous_miss"] += 1
                    current_chat_data[0]["total_miss"] += 1
                    update("chat_stat_faq", str(current_chat_data[0]["_id"]), current_chat_data[0])
                    self.validate_and_escalate(current_chat_data[0]["bot_id"], current_chat_data[0])
                    response_json = self.build_response("string", gettext(
                        'Sorry, I can''t find an answer for that. Could you try again?'), "text", "")
                    self.intentNotFoundRecord(message)
                    return response_json
            else:
                current_chat_data[0]["number_of_continuous_miss"] += 1
                current_chat_data[0]["total_miss"] += 1
                update("chat_stat_faq", str(current_chat_data[0]["_id"]), current_chat_data[0])
                self.validate_and_escalate(current_chat_data[0]["bot_id"], current_chat_data[0])
                response_json = self.build_response("string", gettext(
                    'Sorry, I can''t find an answer for that. Could you try again?'), "text", "")
                self.intentNotFoundRecord(message)
                return response_json

    def checkResponse_non_english(self, message, faq_id, es_analyzer):

        self.session['es_reponse'] = self.search_es_non_english(message, faq_id, es_analyzer)
        self.session['es_reponse'].sort(key=lambda x: int(x[0]), reverse=True)
        current_chat_data = get_all("chat_stat_faq", "session_id", self.session_id)
        current_chat_data[0] = eval(current_chat_data[0])
        if len(self.session['es_reponse']) > 0:

            response_json = self.build_response("string", self.session['es_reponse'][0][3], "text", "")
            response_json_2 = self.feedback(response_json)

            self.session['feedbackFlag_ES'] = True
            update_context(self.session_id, self.session)

            current_chat_data[0]["total_found"] += 1
            current_chat_data[0]["number_of_continuous_miss"] = 0
            update("chat_stat_faq", str(current_chat_data[0]["_id"]), current_chat_data[0])

            return response_json_2
        else:
            current_chat_data[0]["number_of_continuous_miss"] += 1
            current_chat_data[0]["total_miss"] += 1
            update("chat_stat_faq", str(current_chat_data[0]["_id"]), current_chat_data[0])
            self.validate_and_escalate(current_chat_data[0]["bot_id"], current_chat_data[0])
            response_json = self.build_response("string", gettext(
                'Sorry, I can''t find an answer for that. Could you try again?'), "text", "")

            self.intentNotFoundRecord(message)
            return response_json

    def intentNotFoundRecord(self, msg):
        # create interaction record for intent not found
        time = pytz.utc.localize(datetime.datetime.utcnow())
        bot_response = self.session['es_reponse'][0][3] if len(self.session.get("es_reponse", [])) \
            else "Answer Not Found"

        insert('interactions', {
            "message": self.session.get("currentQuestion") or msg,
            "intent_found": 0,
            "log_time": time,
            "bot_id": self.session.get("bot_id"),
            "date_created": time,
            "session_id": self.session_id,
            "subflow_id": self.faq_id,
            "bot_response": bot_response
        })
        self.conv_log.updateIntentNotFound(msg)

    def intentFoundRecord(self, msg):
        self.conv_log.updateIntentFoundCounter(msg)

    def extract_key_phrases(self, text, grammar=r'KT: {(<JJ>* <NN.*>+ <IN>)? <JJ>* <NN.*>+}'):
        # removing stop words and punctuation
        # nltk.download('punkt')
        # nltk.download()
        # nltk.download('averaged_perceptron_tagger')
        punct = set(string.punctuation)
        stop_words = set(nltk.corpus.stopwords.words('english'))

        # tokenize, POS-tag, and chunk using regular expressions
        chunker = nltk.chunk.regexp.RegexpParser(grammar)

        tagged_sents = nltk.pos_tag_sents(nltk.word_tokenize(sent) for sent in nltk.sent_tokenize(text))

        all_chunks = list(itertools.chain.from_iterable(nltk.chunk.tree2conlltags(chunker.parse(tagged_sent))
                                                        for tagged_sent in tagged_sents))

        # join constituent chunk words into a single chunked phrase
        candidates = [' '.join(word for word, pos, chunk in group).lower()
                      for key, group in
                      itertools.groupby(all_chunks, self.lambda_unpack(lambda word, pos, chunk: chunk != 'O')) if key]

        return [cand for cand in candidates
                if cand not in stop_words and not all(char in punct for char in cand)]

    # To automatically unpack the tuples
    @staticmethod
    def lambda_unpack(f):
        return lambda args: f(*args)

    def validate_and_escalate(self, bot_id, dynamic_data):
        print("Data Passed:::::::", dynamic_data)
        try:
            bot = get_by_id("bot_dto", bot_id)
            escalation = None
            if bot:
                if ObjectId.is_valid(bot["esc_id"]):
                    escalation = get_by_id("escalation_dto", bot["esc_id"])
            print(escalation["escalation_trigger_number"])
            if escalation["esc_active_status"] is True and escalation["escalation_trigger_number"] <= dynamic_data[
                "number_of_continuous_miss"]:
                output_report = EscalateData.create_report(self.session_id)
                if isinstance(output_report, str):
                    print("Failed to generate report log")
                    return False
                # result = send_email(output_report, escalation)
                result = add_job_scheduler(output_report, escalation, send_email)
                output_report.close()
                if result == "Success":
                    dynamic_data["number_of_continuous_miss"] = 0
                    update("chat_stat_faq", dynamic_data["_id"], dynamic_data)
                    print("escalation successful")
                else:
                    logger.error(result)
                return True
            else:
                print("No escalation")
                return False
        except Exception as e:
            logger.error(str(e))
            return False

    @staticmethod
    def predict_question_type(question):
        try:
            if question:
                svm = SVMAlgorithm()
                que_type = svm.predict_question_category(question)
                return que_type
        except Exception as e:
            logger.error("Unable to predict question category: {}".format(str(e)))
            return ""
