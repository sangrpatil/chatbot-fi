from apscheduler.schedulers.background import BackgroundScheduler
from apscheduler.jobstores.mongodb import MongoDBJobStore
import datetime
import os
from bson.objectid import ObjectId
from pymongo import MongoClient
import logging

logger = logging.getLogger()

# To select the job-store to store the scheduler jobs

host = os.environ.get('MONGO_HOST') or "localhost"
port = os.environ.get('MONGO_PORT') or 27017
username = os.environ.get('MONGO_USERNAME') or 'chatterboxadmin'
password = os.environ.get('MONGO_PASSWORD') or 'chatterboxadmin'
database = os.environ.get('MONGO_DB') or 'chatterbox'
client = MongoClient('mongodb://'+username+':'+password+'@'+host+':'+str(port)+'/'+database, maxPoolSize=200, waitQueueTimeoutMS=200)
job_stores = {
    'default': MongoDBJobStore(database=database, collection='jobs_escalation', client=client)
}
notification_scheduler = BackgroundScheduler(jobstores=job_stores)


def add_job_scheduler(output_report, escalation, send_mail):
    try:
        print("client")
        from_date = datetime.datetime.now() + datetime.timedelta(seconds=2)
        to_date = datetime.datetime.now() + datetime.timedelta(seconds=12)
        job_id = str(ObjectId())
        notification_scheduler.add_job(send_mail, 'interval', [output_report, escalation],
                                       seconds=15, id=job_id, start_date=from_date, end_date=to_date)
        return "Success"
    except Exception as e:
        print("Error: mail not triggered", str(e))
        return "Failure"