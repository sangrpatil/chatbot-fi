import datetime
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.mime.base import MIMEBase
from email import encoders
import smtplib
import logging

logger = logging.getLogger()

def send_email(report_filename, escalation_data):
    today_date = datetime.datetime.today().strftime('%Y-%m-%d')
    to_addrs = escalation_data["email_recipient"]
    from_addr = 'latam.framework@pfizer.com'

    try:
        msg = MIMEMultipart()
        msg["From"] = from_addr
        msg["To"] = ", ".join(to_addrs)
        msg["Subject"] = escalation_data["email_subject"] + " :: " +str(today_date)
        body = escalation_data["email_content"]
        body = MIMEText(body)  # convert the body to a MIME compatible string
        msg.attach(body)

        part = MIMEBase('application', "octet-stream")
        part.set_payload(report_filename.read())
        encoders.encode_base64(part)
        part.add_header('Content-Disposition', 'attachment; filename=' + "Conversation_Log.xlsx")
        msg.attach(part)
    except Exception as e:
        logger.error("ERROR: Unexpected error: Could not form the message string for email-{}.".format(e))
        return ('Failure')

    try:
        server = smtplib.SMTP('10.128.230.22', local_hostname='mailhub.pfizer.com')
        server.ehlo()
        server.starttls()
        server.sendmail(from_addr, to_addrs, msg.as_string())
        server.quit()
        return ('Success')
    except Exception as e:
        logger.error("ERROR: Unexpected error: Could not send email-{}.".format(e))
        return ('Failure')
