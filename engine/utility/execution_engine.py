from utility.mongo_dao import get_one as get, get_by_id, update
from utility.conversation_logger import ConversationLogger
from fuzzywuzzy import fuzz
from operator import itemgetter
import datetime
import re
import os
import configparser
from flask_babel import gettext
import numpy as np
from utility.mongo_dao import get_by_id, get_all, update, get_one, insert
from utility.session_manager import get as get_context, modify as update_context, set as set_context
from utility.elasticsearch_query import SearchES
from utility.logger import logger
from utility.tts import synthesize_text
from utility.gcp_places import retrieve_location
import json
from bson.objectid import ObjectId
app_config = configparser.ConfigParser()
app_config.read_file(open(r'config/app_settings.ini'))


hybrid_intent_not_found = 'I am not interested in any of these options'


class ExecutionEngine():
    """
        Has three main functions: init, execute, resume
        The rest are utility functions
        @init: initializes the session with an id and starts logging
        @execute: executes bot tasks, i.e. displaying a message or buttons
        @resume: takes user input and determines which next state it matches with. performs a fuzzy search if uncertain
    """
    def __init__(self, session_id, bot_id, chatflow_id):
        self.session_id = session_id
        self.session = get_context(session_id)
        self.conv_log = ConversationLogger(session_id, bot_id)
        self.bot_id = bot_id
        self.chatflow_id = chatflow_id
    """
    Function: executes bot actions by giving user prompts (buttons/text/query outputs)
    This will check whether the output should be text, buttons, etc
    @:param self
    @:param task_definition: this is the current state json
    """
    def get_task_definition(self, search_key, search_value, bot_id):
        state_return = 'None'
        chatflow = get_by_id("chatflow_dto", self.chatflow_id)
        state_definition_all = chatflow.get("mapping")
        for state in state_definition_all:
            if search_key in state.keys():
                if search_key == 'task_id':
                    search_value = int(search_value)
                if state[search_key] == search_value:
                    state_return = state
                    break
        return state_return

    def build_response(self, type, static_text, interaction, interaction_elements, is_multi_message=False, existing_json=None):
        if existing_json is None:
            existing_json = {}
        guidedRestart = False
        if self.session['restartFlag'] and self.session['ES_UsedFlag'] is False:
            guidedRestart = True
        self.conv_log.bot_log(static_text, self.session['ASRFlag'], self.session['endSurveyFlag'], self.session.get("subflow_type"), guidedRestart)
        #sending a message - (multimessage=True, [type,static_text,interaction,interaction_elements])
        static_text = static_text.replace("__NAME__", self.session['name'])
        disable_response = False
        if "button" in interaction or "rating" in interaction:
            disable_response = True
        if self.session['ASRFlag']:
            static_text1 = re.sub(r'<[^<]+?>', ' ', static_text)
            print('STATIC TEXT - ', static_text)
            tts_text = synthesize_text(static_text1)
            asr_use = True
            self.session['ASRFlag'] = False
            update_context(self.session_id, self.session)
        else:
            tts_text = ""
            asr_use = False


        if is_multi_message:
            existing_json['message'].append({
                "interaction elements": interaction_elements,
                "text": static_text,
                "type": type,
                "interaction": interaction,
                "tts": asr_use,
                "tts audio": tts_text,
                "disable_response": disable_response
            })
            existing_json['is_multi'] = True
            return existing_json
        else:
            json_return = {
                "is_multi": False,
                "message": [{
                    "interaction elements": interaction_elements,
                    "text": static_text,
                    "type": type,
                    "interaction": interaction,
                    "tts": asr_use,
                    "tts audio": tts_text,
                    "disable_response": disable_response
                }]
            }
            return json_return

    #     def build_response(
    #         self,
    #         type,
    #         static_text,
    #         interaction,
    #         interaction_elements,
    #         **kwargs

    # ):
    #     """
    #     Builds response dictionary to be send as output
    #     :param type: type of response, text/ button/ url
    #     :param static_text: Text to be shown to user on UI
    #     :param interaction: type of interaction user should be given, button/ string/ multi_select
    #     :param interaction_elements: list of values to be shown to user as choices
    #     :param kwargs: optional arguments to be passed with response
    #     :return: dictionary
    #     """
    #     is_multi_message = kwargs.get('is_multi_message', False)
    #     existing_json = kwargs.get('existing_json', {})
    #     disable_response = kwargs.get('disable_response', False)
    #     good_bye = kwargs.get('good_bye', False)
    #     restart = kwargs.get('restart', False)
    #     show_results = kwargs.get('show_results', False)
    #     data= kwargs.get('data', None)
    #     results_type = kwargs.get("results_type", None)
    #     url= kwargs.get('url', None)
    #     logger.info("In Build Response")

    #     guidedRestart = False
    #     if self.session['restartFlag']  and not self.session['ES_UsedFlag']:
    #         guidedRestart = True

    #     self.conv_log.bot_log(static_text, self.session['ASRFlag'], self.session['endSurveyFlag'], guidedRestart)

    #     if self.session['ASRFlag']:

    #         static_text1 = re.sub(r'<[^<]+?>', ' ', static_text)
    #         #print('STATIC TEXT - ', static_text)
    #         tts_text = synthesize_text(static_text1)
    #         asr_use = True
    #         tts_text = tts_text 
    #         asr_use = True
    #         self.session['ASRFlag'] = False
    #         update_context(self.session_id, self.session)
    #     else:

    #         tts_text = ""
    #         asr_use = False

    #     if is_multi_message:

    #         existing_json['message'].append({
    #             "interaction elements": interaction_elements,
    #             "text": static_text,
    #             "type": type,
    #             "interaction": interaction,
    #             "tts": asr_use or True,
    #             "tts audio": tts_text,
    #             "disable_response": disable_response,
    #             'good_bye': good_bye,
    #             'show_results': show_results,
    #             'restart': restart,
    #             'data': data,
    #             'results_type': results_type,
    #             'url': url

    #         })
    #         existing_json['is_multi'] = True
    #         existing_json['good_bye'] = good_bye,
    #         existing_json['restart'] = restart,
    #         existing_json['show_results'] = show_results,
    #         existing_json['data'] = data
    #         existing_json['results_type'] = results_type
    #         existing_json['url'] = url
    #         return existing_json
    #     else:

    #         json_return = {
    #             "is_multi": False,
    #             "good_bye": good_bye,
    #             'restart': restart,
    #             'show_results': show_results,
    #             'data': data,
    #             "message": [{
    #                 "interaction elements": interaction_elements,
    #                 "text": static_text,
    #                 "type": type,
    #                 "interaction": interaction,
    #                 "tts": asr_use or True,
    #                 "tts audio": tts_text,
    #                 "disable_response": disable_response,
    #                 "good_bye": good_bye,
    #                 'restart': restart,
    #                 'show_results': show_results,
    #                 'data': data,
    #                 'results_type': results_type,
    #                 'url': url
    #             }]
    #         }
    #         return json_return

    def execute(self, task_definition):
        self.session['audioSentFlag'] = False
        self.session['restartFlag'] = False
        self.session['guidedFlag'] = False
        self.session['endSurveyFlag'] = False
        if self.session['on_connect_start']:
            self.session['on_connect_start'] = False
            self.session['disable_input'] = True
        else:
            self.session['disable_input'] = False
        session_id = update_context(self.session_id, self.session)
        new_session = get_context(self.session_id)
        if "parent_id" in list(new_session.keys()) and "subflow_id" in list(new_session.keys()):
            if new_session["chatflow_id"] in new_session["parent_id"]:
                new_session["chatflow_id"] = new_session["subflow_id"]
                update_context(self.session_id, new_session)

        # check what the information about the current state is.
        # depending on what the interaction type is for the current state is, execute accordingly

        try:
            self.conv_log.update(task_definition['task_name'])  # To update the current state in status
            #self.session['botName'] = task_definition['bot_name']
            response_json = ''

            task_text = task_definition['task_text']
            interaction = task_definition['interaction_type']
            # none means the user will just be shown text, and then the conversation will restart
            if interaction == 'none':
                response_json = self.none_execute(task_definition, task_text)

            # if it is url, then show the hyperlink to the user
            elif interaction == 'url':
                response_json = self.url_execute(task_definition, task_text)

            # if it is text, then show the text to the user and wait for their input
            elif interaction == 'text':
                response_json = self.text_execute(task_definition, task_text)

            # if it is a button, give the user buttons to select from
            elif interaction == 'button':
                response_json = self.button_execute(task_definition, task_text)

            # if it is a rating, give the rating buttons to select from
            elif interaction == 'rating':
                response_json = self.rating_execute(task_definition, task_text)

            # if it is a zipcode input, give the user an input to type in a zipcode
            elif interaction == 'zipcode':
                response_json = self.zip_execute(task_definition, task_text)

            # if it is a _____ input, proceed to the next task
            elif interaction == 'nextTask':
                response_json = self.next_task_execute(task_definition, task_text)

            # if it is a subflow, proceed to the subflow, keeping parent flow id in memory
            elif str(interaction).lower().strip() == 'subflow':
                response_json = self.subflow_execute(task_definition, task_text)

            elif interaction in ['productStatusHandler', 'priceHandler', 'customerHandler', 'productInformationHandler']:
                message = self.session['message']
                response_json = self.custom_sheet_execute(message, interaction, task_definition)
        except Exception as e:
            logger.error('Execute Method Error - ', str(e))
            response_json = gettext('Sorry we weren''t able to understand your intent. - EXECUTE Error')
        print(response_json)
        return response_json

    def update_rating(self, session_id, rating):
        json_data = get_all("interactions", "session_id", session_id)
        json_data_last_update = eval(json_data[-1])
        json_data_last_update["Rating"] = rating
        id_interation = str(json_data_last_update["_id"])
        update("interactions", id_interation, json_data_last_update)


    def resume(self, message, ASR, bot_id):
        # FI - graph fix
        match_phrase = self.get_phrases_map(message)
        if message in ["Product", "Party Type", "Domicile Country", "Delinquent Days", "Period"]:
            response = self.get_multiselect_options(message, self.session_id)
            return response
        elif message == "Show Results":
            response = self.get_graph(message, self.session_id)
            return response
        elif match_phrase:
            response = self.get_graph(message, self.session_id, match_phrase)
            return response
        # elif message in 
        # elif message == "Trend over last 4 quarters":
        #     response = self.get_graph(message, self.session_id, True)
        #     return response
        
        if message == "Survey":
            response = self.execute_survey(bot_id, message)
            return response

        # store filter_stages
        stage_messages = {
            'INFOLEASE': 'stage_1', 'HELMS': 'stage_1', 'HOGAN': 'stage_1',
            'Outstanding Balance': 'stage_2', 'Number of loans': 'stage_2',
            'EOP balance': 'stage_2', 'Number of deposits': 'stage_2',
            'Trend over last 4 quarters': 'stage_2'
        }
        if message in list(stage_messages.keys()):
            stage = message.lower().replace(" ", "_")
            print("Update This ###########", [stage_messages.get(message), stage])
            self.update_stage_filter(stage_messages.get(message), stage, self.session_id)

        #todo - should I make the returns uniform
        self.session['message'] = message
        if self.session['on_connect_start']:
            self.session['on_connect_start'] = False
            self.session['disable_input'] = True
        else:
            if str(message).lower().strip() == 'nein' and self.session['disable_input']:
                self.session['disable_input'] = True
            else:
                self.session['disable_input'] = False
        self.session['ASRFlag'] = ASR
        logger.debug('THE ASR VALUE IS = ', self.session['ASRFlag'], ASR)
        self.session['message'] = message
        self.session['audioSentFlag'] = False
        self.session['audioSentFlag_ES'] = False
        self.session['ES_UsedFlag'] = False
        update_context(self.session_id, self.session)

        self.conv_log.user_log(message, self.session['ASRFlag'], self.session['endSurveyFlag'], self.session.get("subflow_type"), guidedRestartFlag=False)
        try:
            # first check if the message is small talk
            small_talk_response = self.small_talk_module(message, bot_id)
            keyword_response = self.keyword_handler(message, bot_id)
            if small_talk_response:
                return small_talk_response
            elif keyword_response:
                return keyword_response
            # next check if we are expecting a yes/no in response to restarting
            elif self.session['restartFlag'] is True:
                return self.restart_execute(message, self.session_id, self.session, bot_id)
            elif self.session['restartFlagFAQ'] is True:
                return self.restart_execute_FAQ(message, self.session_id, self.session, bot_id)
            elif self.session['restartFlagNextTask'] is True:
                return self.restart_next_task_execute(message, self.session_id, self.session, bot_id)
            elif self.session['endSurveyFlag'] is True:
                return self.survey_execute(message)
            # next check if we are expecting a yes/no in response to fuzzy search
            elif self.session['fuzzyFlag'] is True:
                return self.fuzzy_execute(message, bot_id)
            # next check if we're expecting a response from a hybrid guided
            elif self.session['guidedFlag'] is True:
                return self.guided_flag_execute(message, bot_id)
            # if it was none of the above, then it is a normal message
            else:
                # language and analyzer
                es_analyzer = get_by_id('bot_dto', bot_id)['es_analyzer']

                # first determine what the state is in the conversation, and what states can lead from here
                current_state = get_by_id('status', self.session_id)['current_state']

                state_definition = self.get_task_definition('task_name', current_state, bot_id)
                action_type = state_definition['action_type']
                next_task_ids = state_definition['next_task_ids']
                current_interaction_type = state_definition["interaction_type"]
                next_possible_states_json = []
                next_possible_states_names = []
                if current_interaction_type == "rating":
                    print("Update Rating")
                    self.update_rating(self.session_id, message)
                try:
                    for id in next_task_ids:
                        state_option = self.get_task_definition('task_id', id, bot_id)
                        next_possible_states_json.append(state_option)
                    for state_json in next_possible_states_json:
                        next_possible_states_names.append(state_json['task_name'])
                except Exception as e:
                    logger.error("Execution engine error: ", str(e))
                    next_possible_states_json = self.get_task_definition('is_start', True, bot_id)
                    next_possible_states_names = next_possible_states_json['task_name']



                # if the action type is code, take the user's message and apply it to the code
                if action_type == 'code':
                    self.codeExecute(message, state_definition)
                # if the action if FAQ, then take the user's message and query elasticsearch with it
                elif action_type == 'FAQ':
                    session = get_context(self.session_id)
                    response = self.faq_execute(message, bot_id, session["faq_id"], self.session['ASRFlag'])
                    return response
                # if the action is hybrid guided, take the user's message and check the hybrid guided process
                elif action_type == 'hybridGuided':
                    response = self.hybrid_guided_execute(message, next_possible_states_names, bot_id)
                    return response
                # if the action is query, query the user's message against a database using DAO.py
                elif action_type == 'query':
                    self.queryExecute(message, current_state)
                # if the action type is navigate, we are navigating from one branch to another in the conversation
                elif action_type == 'navigate':
                    response = self.navigate_execute(message, next_possible_states_names, bot_id)
                    return response
                # if the action type is location, we are taking in the user's zip code input
                elif action_type == 'location':
                    response = self.location_execute(message)
                    return response
                elif action_type == 'input':
                    response = self.input_execute(message, next_possible_states_names)
                    return response
                #elif action_type == "subflow":
                #    response = self.subflow_execute(state_definition, state_definition['task_text'])
                else:
                    response = self.faq_execute(message, bot_id, state_definition["subflow_id"],
                                                self.session['ASRFlag'])
                    return response
        except Exception as e:
            logger.error(str(e))
            return gettext('Sorry, we weren''t able to understand your intent - RESUME Error')

    #Execute Functions

    def subflow_execute(self, task_definition, task_text):
        currentState = task_definition['task_name']
        nextState = task_definition['next_task_ids']
        session = get_context(self.session_id)

        if "subflow_type" in list(task_definition.keys()):
            session["subflow_type"] = task_definition["subflow_type"]
            if str(task_definition["subflow_type"]).lower().strip() == "faq":
                # print(get_by_id('bot_dto', session["bot_id"]))
                session["faq_id"] = task_definition["subflow_id"]
                update_context(self.session_id, session)
                self.session["faq_id"] = task_definition["subflow_id"]
                es_analyzer = get_by_id('bot_dto', session["bot_id"])['es_analyzer']
                response = self.faq_execute(session["message"], session["bot_id"], task_definition["subflow_id"], self.session['ASRFlag'])
                return response
            else:
                pass

        if "parent_id" not in list(session.keys()):
            session["parent_id"] = [session["chatflow_id"]]
        else:
            session["parent_id"].append(session["chatflow_id"])
        if "parent_task" not in list(session.keys()):
            session["parent_task"] = [task_definition["task_id"]]
        else:
            session["parent_task"].append(task_definition["task_id"])
        session["chatflow_id"] = task_definition["subflow_id"]
        session["current_flow"] = "subflow"
        session["subflow_id"] = task_definition["subflow_id"]
        update_context(self.session_id, session)
        self.session = session
        message = re.sub(r'<>', currentState, task_text[0])
        response = self.build_response("string", message, "text", "")
        subflow = get_by_id("chatflow_dto", session["chatflow_id"])
        # print("Subflow::", subflow)
        for element in subflow["mapping"]:
            if str(element["is_start"]).lower().strip() == "true":
                return self.execute(element)
        return response

    def next_task_execute(self, task_definition, task_text):
        currentState = task_definition['task_name']
        nextState = task_definition['next_task_ids'][0]
        message = re.sub(r'<>', currentState, task_text[0])
        logger.debug('next state - ', nextState, 'bot ID - ', self.session['bot_id'])

        response = self.build_response("string", message, "text", "")

        next_task_def = self.get_task_definition("Task ID", nextState, self.session['bot_id'])
        response_json_2 = self.execute(next_task_def)

        response['is_multi'] = True
        response['message'].append(response_json_2['message'][0])
        return response

    def text_execute(self, task_definition, task_text):
        if str(task_definition['interaction_fetch_from_db']).lower() != "true":
            response_task_text = self.build_response('string', task_text[0], 'text', '')
            return response_task_text
        else:
            response_task_text = self.build_response('string', task_text[0], 'text', '')
            print(response_task_text)
            return response_task_text

    def button_execute(self, task_definition, task_text):  # type, static_text, interaction, interaction_elements
        currentState = task_definition['task_name']
        task_text = re.sub(r'<>', currentState, task_text[0])
        if task_definition['interaction_fetch_from_db'] != "True":
            interaction_values = task_definition['interaction_values']
            task_text = self.build_response('list', task_text, 'button_vertical', interaction_values)
            return task_text

    def rating_execute(self, task_definition, task_text):  # type, static_text, interaction, interaction_elements
        currentState = task_definition['task_name']
        task_text = re.sub(r'<>', currentState, task_text[0])
        if str(task_definition['interaction_fetch_from_db']) != "True":
            interaction_values = task_definition['interaction_values']
            task_text = self.build_response('list', task_text, 'button_horizontal', interaction_values)
            return task_text

    # if interaction is none, check if query database or not
    # just show text and then restart
    def none_execute(self, task_definition, task_text):
        # global results
        static_values = task_definition['interaction_values']
        # print from flashmessage if interaction = none
        if str(task_definition['interaction_fetch_from_db']).lower() != "true":
            currentState = task_definition['task_name']
            message = re.sub(r'<>', currentState, task_text[0])
            response = self.build_response("string", message, "text", "")
            response_json_2 = self.restart(response)
            return response_json_2

    # if interaction is url, check if query database or not
    # then display url and restart
    def url_execute(self, task_definition, task_text):  # , dbData):
        interaction_url = task_definition['interaction_values'][0]
        if str(task_definition['interaction_fetch_from_db']).lower() != "true":
            currentState = task_definition['task_name']
            message = re.sub(r'<>', currentState, task_text[0])
            response = self.build_response("hyperlink", task_text[0], "url", interaction_url)

            current_state = get_by_id('status', self.session_id)['current_state']
            bot_id = self.session['bot_id']
            state_definition = self.get_task_definition('task_name', current_state, bot_id)

            next_state = state_definition['next_task_ids'][0]
            next_state_def = self.get_task_definition('task_id', next_state, bot_id)
            response_json_2 = self.execute(next_state_def)

            response['is_multi'] = True
            response['message'].append(response_json_2['message'][0])
            return response

    def zip_execute(self, task_definition, task_text):
        response = self.build_response("string", task_text[0], "location_zip", "")
        return response

    #Resume Functions

    def small_talk_module(self, msg, bot_id):
        try:
            small_talk_id = get_by_id('bot_dto', bot_id).get('small_talk_id')
            if not small_talk_id:
                return 0
            small_talk = get_by_id('small_talk_dto', small_talk_id)
            questionnaire = small_talk["questionnaire"]
            msg = re.sub(r'[^\w\s]', '', msg)
            if not questionnaire:
                return 0

            for que in questionnaire:
                question_txt = que.get('question').lower().strip()
                if msg.lower().strip() == gettext(question_txt) or \
                    fuzz.ratio(msg, question_txt) > 65:
                    return self.build_response("string", que.get('response'), "text", "")
        except Exception as e:
            return 0

    def keyword_handler(self, message, bot_id):

        help_config = app_config.get('GENERAL', 'help_keywords')
        restart_config = app_config.get('GENERAL', 'restart_keywords')
        goback_config = app_config.get('GENERAL', 'goback_keywords')

        help_keywords = help_config
        restart_keywords = restart_config
        goback_keywords = goback_config
        help_found = False
        restart_found = False
        goback_found = False

        if isinstance(help_keywords, str):
            help_keywords = help_keywords.replace("[","").replace("]", "").replace("'", "").split(",")
            for element in help_keywords:
                if element.lower().strip() == message.lower().strip():
                    help_found = True
        if isinstance(restart_keywords, str):
            restart_keywords = restart_keywords.replace("[","").replace("]", "").replace("'", "").split(",")
            for element in restart_keywords:
                if element.lower().strip() == message.lower().strip():
                    restart_found = True
        if isinstance(goback_keywords, str):
            goback_keywords = goback_keywords.replace("[","").replace("]", "").replace("'", "").split(",")
            for element in goback_keywords:
                if element.lower().strip() == message.lower().strip():
                    goback_found = True

        if help_found:
            help_task = self.get_task_definition("Task Name", "help", bot_id)
            if help_task != "None":
                help_text = help_task['task_text']
            else:
                help_task = gettext("type restart to re-start the chat, Click on the given options or type your query.")
                response = self.build_response("string", help_task, "text", "")
                return response
            response = self.build_response("string", help_text, "text", "")


            next_state = help_task['next_task_ids']

            if next_state == '' or next_state == [] or next_state == [""]:
                return response

            next_state_def = self.get_task_definition('task_id', next_state[0], bot_id)
            response_json_2 = self.execute(next_state_def)

            response['is_multi'] = True
            response['message'].append(response_json_2['message'][0])
            return response

        elif restart_found:

            context = get_context(self.session_id)

            if "parent_id" in list(context.keys()):
                if len(context["parent_id"]) > 0:
                    if ObjectId.is_valid(context["parent_id"][0]):
                        context["chatflow_id"] = context["parent_id"][0]
                        self.session["chatflow_id"] = context["parent_id"][0]
                        self.chatflow_id = context["chatflow_id"]
                        self.session["parent_id"] = []
                        self.session["parent_task"] = []
                        context["parent_id"] = []
                        context["parent_task"] = []
                        update_context(self.session_id, context)
            restart = self.get_task_definition("is_start", True, bot_id)
            # self.session['custStatus'] = 'Done'
            # self.session['productInformationHandler'] = 'Done'
            # self.session['priceStatus'] = 'Done'
            # self.session['prodStatus'] = 'Done'
            update_context(self.session_id, self.session)
            print(restart)
            return self.execute(restart)
        elif goback_found:
            current_state = get_by_id('status', self.session_id)['current_state']
            prior_state = get_by_id('status', self.session_id)['prior_state']
            history = get_by_id('status', self.session_id)['history']
            history = history[:-1] #pop out the last element
            if (len(history) == 0):
                return self.build_response("string", gettext("Sorry, there is nothing to go back to."), "text", "")
            # get the last element
            previous_state = history[-1]
            # pop out again because will be replaced in conversation logger
            history = history[:-1]
            update('status', self.session_id, {'current_state': previous_state, 'prior_state': current_state, 'history': history, "date_created": datetime.datetime.now()})
            previous_state_json = self.get_task_definition('task_name', previous_state, bot_id)
            return self.execute(previous_state_json)
        else:
            return 0

    def survey_execute(self, message):
        # TODO - need to execute it in survey subflow
        if self.session['surveyFlagNum'] is True:
            if message in '12345':
                # log survey
                self.session['endSurveyFlag'] = True
                self.session['surveyFlagNum'] = False
                update_context(self.session_id, self.session)
                response = self.survey_create(2, '')
                return response
            else:
                response = self.build_response("string", gettext('Please choose an option 1-5.'), "text", "")
                response_json_2 = self.survey_create(1,response)
                return response_json_2
        elif self.session['surveyFlagComm'] is True:
            if message.strip().lower() == gettext('yes'):
                response = self.build_response("string", gettext('Please leave your comment below:'), "text", "")
                return response
            else:
                self.session['endSurveyFlag'] = False
                self.session['surveyFlagComm'] = False
                self.session['surveyFlagNum'] = False
                update_context(self.session_id, self.session)
                response = self.build_response("string", gettext('Thank you for your participation!'), "text", "")
                return response

    def survey_create(self,state,json):

        self.session['endSurveyFlag'] = True
        update_context(self.session_id, self.session)
        if state ==1:
            self.session['surveyFlagNum'] = True
            update_context(self.session_id, self.session)
            options = [1,2,3,4,5]
            response = self.build_response("list",gettext('On a scale of 1-5, how satisfied were you with your service today?'),"button_horizontal",options,True,json)
            return response
        elif state == 2:
            self.session['surveyFlagComm'] = True
            update_context(self.session_id,self.session)
            response = self.build_response("list",gettext('Would you like to leave any comments?'),"button_horizontal",[gettext("Yes"), gettext("No")])
            return response

    # use Fuzzywuzzy to check for fuzzysearch options for Navigate options
    # if the match is > 45% similar, return as option for user to pick from
    def fuzzy_search(self, nextStates, message):
        options = []
        for state in nextStates:
            stateOutput = re.sub(r'<.*>', '', state)
            options.append([fuzz.ratio(message, stateOutput), state, stateOutput])
        self.session['suggestion'] = max(options, key=itemgetter(0))[1]
        suggestionOutput = max(options, key=itemgetter(0))[2]
        ratio = max(options, key=itemgetter(0))[0]
        if ratio > int(os.environ.get('FUZZY_RATIO') or '45'):
            # check if task is a navigate task, if yes, check message value, see if pass
            response = self.build_response("list",gettext('We couldn\'t find that. Do you mean: ') + suggestionOutput,"button_vertical",[gettext("Yes"), gettext("No")])
            self.session['fuzzyFlag'] = True
            update_context(self.session_id, self.session)
            return response
        else:
            response = self.build_response("string",gettext('Sorry we don\'t have an answer for that. Could you try again?'),"text","")
            return response

    # If FuzzyFlag is True, then execute this
    # check if the fuzzysearch came from a query or code
    # if not, then check if the suggestion picked comes from a next state
    # if not what they are looking for, return to start
    def fuzzy_execute(self, message, bot_id):
        if str(message).lower() == gettext('yes'):
            self.session['fuzzyFlag'] = False
            get_next_state = self.get_task_definition('task_name',self.session['suggestion'],bot_id)
            response = self.execute(get_next_state)
            update_context(self.session_id,self.session)
            return response
            pass
        elif message.lower() == gettext('no'):
            self.session['fuzzyFlag'] = False
            update_context(self.session_id,self.session)
            next_state = self.get_task_definition('is_start',True,bot_id)
            response = self.execute(next_state)
            return response
        else:
            response = self.build_response("list",gettext('Please choose yes or no.'),"button_horizontal",[gettext("Yes"), gettext("No")])
            return response

    def input_execute(self, message, next_possible_state_names):
        if self.session['name'] == 'None': #todo store these in conversation logger
            self.session['name'] = message
        elif 'contact' not in self.session.keys():
            self.session['contact'] = message
        elif 'profession' not in self.session.keys():
            self.session['profession'] = message
        else:
            pass #add something here later if all fields are input
        update_context(self.session_id, self.session)
        get_next_state = self.get_task_definition('task_name', next_possible_state_names[0], self.session['bot_id'])
        response = self.execute(get_next_state)
        return response


    def navigate_execute(self, message, next_possible_states_names, bot_id):
        found = False
        for state in next_possible_states_names:
            stateCompare = re.sub(r'<.*>', '', state)

            # FI work-around
            stateCompare_list = [ st.strip().lower() for st in stateCompare.split(";") ]
            if message.strip().lower() in stateCompare_list:
                get_next_state = self.get_task_definition('task_name', state, bot_id)
                print('NAVIGATE NEXT STATE - ', get_next_state)
                if 'None' in str(get_next_state):
                    pass
                else:
                    found = True
                    response = self.execute(get_next_state)
                    return response

        if found is False:
            # do Fuzzy search here
            response = self.fuzzy_search(next_possible_states_names, message)
            return response
            pass

    def get_start_mapping_from_current_flow(self, session_id):
        session = get_context(self.session_id)
        chatflow_mapping = get_by_id("chatflow_dto", session["chatflow_id"])["mapping"]
        for element in chatflow_mapping:
            if str(element["is_start"]).lower().strip() == "true":
                return element
        return None

    def restart_execute(self, message, session_id, session, bot_id):
        if message.strip().lower() == gettext('yes'):
            session['restartFlag'] = False
            session['queryFlag'] = False
            original_session = get_context(session_id)
            original_session['restartFlag']  =session['restartFlag']
            original_session['queryFlag'] = session['queryFlag']

            if "parent_id" in list(original_session.keys()):
                if len(original_session["parent_id"]) != 0:
                    original_session["chatflow_id"] = original_session["parent_id"][0]
                    original_session["current_flow"] = "main flow"
                    original_session["parent_id"] = []
                    if "parent_task" in list(original_session.keys()):
                        original_session["parent_task"] = []
                    original_session["subflow_id"] = original_session["chatflow_id"]
            self.session = original_session
            update_context(session_id, original_session)
            get_start = self.get_start_mapping_from_current_flow(self.session_id)
            # get_start = self.get_task_definition("Is Start", "true", bot_id)
            start_response = self.execute(get_start)
            return start_response

        elif message.strip().lower() == gettext('no'):
            session['restartFlag'] = False
            session['queryFlag'] = False
            session_id = update_context(session_id, session)
            response_first = self.build_response("string", gettext('Thanks for chatting, hope to talk again soon!'), "text", "", False)
            # response_final = self.survey_create(1, response_first)
            update('status', self.session_id, {'has_ended': True})
            return response_first
        else:
            response = self.build_response("list", gettext('Would you like to continue? Please choose yes or no.'), "button_horizontal",[gettext("Yes"), gettext("No")])
            return response

    def restart_execute_FAQ(self, message, session_id, session, bot_id):

        if message.strip().lower() == gettext('yes'):
            session['restartFlagFAQ'] = False
            session['queryFlag'] = False
            session_id = update_context(session_id, session)

            start_response = self.build_response("string",gettext("What other questions can I help you with?"),"text","",False)
            return start_response

        elif message.strip().lower() == gettext('no'):
            session['restartFlagFAQ'] = False
            session['queryFlag'] = False
            session_id = update_context(session_id, session)
            response_first = self.build_response("string", gettext('Thanks for chatting, hope to talk again soon'), "text", "",False)
            #response_final = self.survey_create(1, response_first)
            update('status', self.session_id, {'has_ended': True})
            return response_first
        else:
            response = self.build_response("list", gettext('Would you like to continue? Please choose yes or no.'),
                                           "button_horizontal", [gettext("Yes"), gettext("No")])
            return response

    def restart_next_task_execute(self, message, session_id, session, bot_id):

        if message.strip().lower() == gettext('yes'):
            session['restartFlagNextTask'] = False
            session['queryFlag'] = False
            session_id = update_context(session_id, session)
            current_state = get_by_id('status', self.session_id)['current_state']

            state_definition = self.get_task_definition('task_name', current_state, bot_id)

            next_state = state_definition['next_task_ids'][0]
            next_state_def = self.get_task_definition('task_id', next_state, bot_id)
            start_response = self.execute(next_state_def)
            return start_response

        elif message.strip().lower() == gettext('no'):
            session['restartFlagNextTask'] = False
            session['queryFlag'] = False
            session_id = update_context(session_id, session)
            response_first = self.build_response("string", gettext('Thanks for using the Pfizer Bot!'), "text", "",
                                                 False)
            response_final = self.survey_create(1, response_first)
            update('status', self.session_id, {'has_ended': True})
            return response_final
        else:
            response = self.build_response("list", gettext('Would you like to continue? Please choose yes or no.'),
                                           "button_horizontal", [gettext("Yes"), gettext("No")])
            return response

    # if action type is hybrid, execute this
    # first check if the message is a 1:1 match with taskNames
    # if it is, execute the corresponding state
    # if it is not, check message against keywords. For each keyword matching in the message, add a point
    # return taskNames with highest keyword scores for using to pick from
    def hybrid_guided_execute(self, message, next_possible_states_names, bot_id):
        # global guidedFlag
        found = False

        for state in next_possible_states_names:
            stateCompare = re.sub(r'<.*>', '', state)
            if message.strip().lower() == stateCompare.strip().lower():
                #get_next_state = get('state', 'state_name', state)
                get_next_state = self.get_task_definition('task_name', state, bot_id)
                if 'None' in str(get_next_state):
                    pass
                else:
                    found = True
                    self.conv_log.updateIntentFoundCounter(message)
                    interaction = get_next_state['interaction_type']
                    response = self.execute(get_next_state)
                    print('interaction is ', interaction)
                    if 'handler' not in interaction.lower():
                        response2 = self.restart_next_task(response)
                        return response2
                    else:
                        return response
        if found is False:  # so here instead of a fuzzysearch, do the keyword search
            stateScores = []
            finalStates = []
            possibleNonAliasStates = []
            for state in next_possible_states_names:
                score = 0
                stateinfo = self.get_task_definition('task_name', state, bot_id)
                # print('STATE INFO HYBRID - ', stateinfo)
                alias = stateinfo['alias']
                if alias.lower() == 'false':
                    keywords = stateinfo['keywords']
                    if len(keywords) > 0:
                        print('list of keywords ', keywords)
                        for key in keywords:
                            if key.lower().strip() in message.lower().strip() and key != '':
                                score += 1
                                print('KEY IS -', key,'-')
                                # if key.lower().strip() in savedKey.lower().strip() -> score+=1
                        stateScores.append(score)
                        possibleNonAliasStates.append(state)
            print('STATE SCORES - ', stateScores)
            print('STATE ALIASES - ', possibleNonAliasStates)
            # pick the state names with the highest scores, return those as options
            highScore = max(stateScores)
            print('HIGH SCORE - ', highScore)
            if highScore != 0:
                for i in np.arange(0, len(stateScores)):
                    if stateScores[i] == highScore:
                        finalStates.append(possibleNonAliasStates[i])
            print('FINAL STATES - ', finalStates)
            if len(finalStates) != 0:
                if len(finalStates) == 1:
                    get_next_state = self.get_task_definition('task_name', finalStates[0], bot_id)
                    self.conv_log.updateIntentFoundCounter(message)  #todo check what the interaction type was. if it was normal or custom
                    response = self.execute(get_next_state)
                    interaction = get_next_state['interaction_type']
                    print('interaction is ', interaction)
                    if 'handler' not in interaction.lower():
                        response2 = self.restart_next_task(response)
                        return response2
                    else:
                        return response
                else:
                    print('babel test- ', gettext(hybrid_intent_not_found))
                    finalStates.append(gettext(hybrid_intent_not_found))
                    response = self.build_response("list", gettext('Are you interested in any of these options?'), "button_vertical",finalStates)
                    self.session['guidedFlag'] = True
                    self.session['hybrid_user_intent'] = message
                    session_id = update_context(self.session_id, self.session)
                    print('RESPONSE - ', response)
                    return response
            else:
                response = self.build_response("string", gettext('Sorry, we may not have information for that. Can you try again?'),"text","")
                return response

    # if expecting a response from Hybrid guided, check here if it leads to a valid state
    def guided_flag_execute(self, message, bot_id):
        get_next_state = self.get_task_definition('task_name', message, bot_id)
        if str(get_next_state) != 'None':
            response = self.execute(get_next_state)
            self.session['guidedFlag'] = False
            session_id = update_context(self.session_id, self.session)
            self.conv_log.updateIntentFoundCounter(self.session['hybrid_user_intent'])
            return response
        elif message == gettext(hybrid_intent_not_found):
            self.conv_log.updateIntentNotFound(self.session['hybrid_user_intent'])
            response = self.build_response("string", gettext('I\'m sorry none of those options were helpful. Would you like to try rephrasing your question?'), "text", "")
            response_json_2 = self.restart(response)
            self.session['guidedFlag'] = False
            update_context(self.session_id, self.session)
            return response_json_2
        else:
            response = self.build_response("string", gettext('Please choose one of the options'),"text","")
            return response

    def faq_execute(self, message, bot_id, faq_id, asr_flag):
        print("----------------------------")
        print("To be searched in FAQ:", message, faq_id)
        print("----------------------------")
        search = SearchES(self.session_id, bot_id, faq_id)
        self.session['ES_UsedFlag'] = True
        update_context(self.session_id, self.session)
        response_flag, response = search.execute(message, bot_id, faq_id, asr_flag)
        print("Response Flag:", response_flag)
        print("Response Received:", response)
        if response_flag == 0:
            self.session['ASRFlag'] = False
            self.session['suggestCounter_ES'] = 0
            self.session['feedbackFlag_ES'] = False
            response_json_2 = self.restart_FAQ(response)
            update_context(self.session_id, self.session)
            return response_json_2
        # self.restart()
        elif response_flag == 1:
            #display the ES answer
            logger.debug('RESPONSE TO RETURN FROM FAQ EXECUTE - ', response)
            return response
        elif response_flag == 2:
            state = self.get_task_definition('is_start', True,bot_id)
            response = self.execute(state)
            return response
        if response_flag == 3:
            self.session['ASRFlag'] = False
            self.session['suggestCounter_ES'] = 0
            self.session['feedbackFlag_ES'] = False
            response_json_2 = self.restart_FAQ(response)
            update_context(self.session_id, self.session)
            return response_json_2
        pass

    def location_execute(self, message):
        try:
            #message should be a 5 digit zip code
            pharmacy_data = retrieve_location(message, "pharmacy", 5)
            if pharmacy_data['status'] == 'Success':
                pharmacy_list = pharmacy_data['message']
                response = self.build_response("list", gettext("Please select any of the stores to view it on a map"), "location_maps", pharmacy_list)  # [[name, address],[name, address]]

            else:
                response = self.build_response("string", gettext("Sorry, we do not have pharmacies within 50 miles of your area."),"text", "")

            current_state = get_by_id('status', self.session_id)['current_state']
            bot_id = self.session['bot_id']
            state_definition = self.get_task_definition('task_name', current_state, bot_id)

            next_state = state_definition['next_task_ids'][0]
            next_state_def = self.get_task_definition('task_id', next_state, bot_id)
            response_json_2 = self.execute(next_state_def)

            response['is_multi'] = True
            response['message'].append(response_json_2['message'][0])

            return response
        except Exception as e:
            logger.error("Location Execute Error:",str(e))


    # ask the user if they would like to restart - send button and set global restartflag to True
    def restart(self, json):
        try:
            response = self.build_response("list", gettext("Would you like to continue?"),"button_horizontal",[gettext("Yes"), gettext("No")], True, json)
            self.session['restartFlag'] = True
            session_id = update_context(self.session_id, self.session)
            return response
        except Exception as e:
            logger.error("Restart Error: ",str(e))

    # ask the user if they would like to restart - send button and set global restartflag to True
    def restart_FAQ(self, json):
        try:
            response = self.build_response("list", gettext("Would you like to continue?"), "button_horizontal",
                                           [gettext("Yes"), gettext("No")], True, json)
            self.session['restartFlagFAQ'] = True
            session_id = update_context(self.session_id, self.session)

            return response
        except Exception as e:
            logger.error("Restart FAQ Error: ",str(e))

    def restart_next_task(self, json):
        try:
            response = self.build_response("list", gettext("Would you like to continue?"), "button_horizontal",
                                           [gettext("Yes"), gettext("No")], True, json)
            self.session['restartFlagNextTask'] = True
            session_id = update_context(self.session_id, self.session)
            return response
        except Exception as e:
            logger.error("Restart Next Task Error: ",str(e))

    def execute_survey(self, bot_id, message):
        try:
            response = self.build_response("string", gettext('I\'m sorry Survey is not configured.'), "text", "")
            session = get_context(self.session_id)
            survey_id = get_by_id("bot_dto", bot_id).get("survey_id", "")
            survey_mapping = get_by_id("chatflow_dto", survey_id)["mapping"]
            print("executing survey .................", survey_id)
            if survey_id:
                session["chatflow_id"] = survey_id
                session["current_flow"] = "subflow"
                session["subflow_id"] = survey_id
                session["subflow_type"] = "Survey"
                update_context(self.session_id, session)
                self.session = session
                for element in survey_mapping:
                    if str(element["is_start"]).lower().strip() == "true":
                        response = self.execute(element)
        except Exception as e:
            response = self.build_response("string", gettext('I\'m sorry Survey is not configured.'), "text", "")
        finally:
            return response

    def get_graph(self, message, session_id, only_graph=False):
        response = {
            "is_multi": True,
            "graph_data": "",
            "show_results": "true",
            "message": []
        }

        filter_msg = {
            'disable_response': True, 'interaction': 'button_vertical',
            'interaction elements': ['Product', 'Party Type', 'Domicile Country',
            'Period', 'Show Results', 'Restart'],
            'text': 'Please select filter criteria',
            'tts': False,
            'tts audio': '',
            'type': 'list'
        }

        restart_msg = {'interaction elements': ['Credit portfolio', 'Deposit portfolio'], 'text': 'Hello, Welcome to the GC Assist.Please Choose the portfolio to proceed.\n', 'type': 'list', 'interaction': 'button_vertical', 'tts': False, 'tts audio': '', 'disable_response': True}
        if only_graph:
            graph_data = get_one("graph", "for_metrics", only_graph)
            del graph_data["_id"]
            data = {
                'chart_data': graph_data, 'show_results': 'true',
                'disable_response': True, 'interaction': 'text',
                'interaction elements': [], 'text': '', 'tts': False,
                'tts audio': '', 'type': 'list'
            }
            response["message"] = [data, restart_msg]
            return response
        else:
            filters_data = get_one("filters_dto", "session_id", session_id)
            if filters_data:
                print("filters_data found #######################", filters_data)
                del filters_data["_id"]
                selected_filters = list(filters_data.keys())
                print("Selected filters ###############", selected_filters)

                if "Period" in selected_filters:
                    for_metrics = "infolease"
                    if "stage_1" in selected_filters and "stage_2" in selected_filters:
                        for_metrics = filters_data.get("stage_1") +"-"+ filters_data.get("stage_2")
                        print("for_metrics ############################", for_metrics)
                    graph_data = get_one("graph", "for_metrics", for_metrics)
                    if not graph_data:
                        graph_data = get_one("graph", "for_metrics", "infolease")
                    del graph_data["_id"]
                    data = {
                        'chart_data': graph_data, 'show_results': 'true',
                        'disable_response': True, 'interaction': 'text',
                        'interaction elements': [], 'text': '', 'tts': False,
                        'tts audio': '', 'type': 'list'
                    }
                    response["message"] = [data, restart_msg]
                else:
                    print("mandatory fields missing ############################")
                    filter_msg["text"] = "Sorry, Period is mandatory filters.Please select filter criteria"
                    regret_msg = {
                        "message": [filter_msg],
                        "is_multi": False 
                    }
                    response.update(regret_msg)
            else:
                print("filters_data missing ##################################")
                filter_msg["text"] = "Sorry, Period is mandatory filters.Please select filter criteria"
                regret_msg = {
                    "message": [filter_msg],
                    "is_multi": False 
                }
                response.update(regret_msg)
            return response

    def get_multiselect_options(self, message, session_id):
        option_mapper = {
            "Product": ["All"],
            "Party Type": ["INDI", "CORP", "GOV"],
            "Domicile Country": ["US", "NON US"],
            "Delinquent Days": ["30", "60", "90"],
            "Period": ["Last Month End", "Last Quarter end", "Last year end"]
        }

        filters_data = get_one("filters_dto", "session_id", session_id)
        if filters_data:
            print("###########filters_ selected#########", filters_data)
            del filters_data["_id"]
            del filters_data["session_id"]
            filter_ = message.split(" ")[0]
            filter_values = filters_data.get(filter_, [])
            interaction_values = [{'value': val, 'selected': True if val in filter_values else ''} for val in option_mapper.get(message)]
        else:
            interaction_values = [{'value': val, 'selected':''} for val in option_mapper.get(message)]
        response = {
            "is_multi": False,
            "message": [{
                "disable_response": "true",
                "interaction": "button_multiselect",
                "interaction elements": interaction_values,
                "text": "Select {}".format(message),
                "tts": "false",
                "tts audio": "",
                "type": "list"
            }]
        }
        return response

    def update_stage_filter(self, stage, selection, session_id):
        filters_data = get_one("filters_dto", "session_id", session_id)
        data = {stage:selection}
        if not filters_data:
            data["session_id"] = session_id
            insert("filters_dto", data)
        else:
            update("filters_dto", filters_data["_id"], data)

    def get_phrases_map(self, message):
        phrases_map = {
            "Give me total Outstanding Loans by Product for Party 'INDI' and Domicile country 'US' for a specific Period 'Dec-2019' for infolease": "infolease-outstanding_balance",
            "Get me number of Loans by Product for Party 'INDI' and Domicile country 'US' for a specific Period 'Dec-2019' for infolease": "infolease-number_of_loans",
            "Fetch me trend of Outstanding Loans and number of Loans by Product for Party INDI' and Domicile country 'US' across last four quarters for infolease": "infolease-trend_over_last_4_quarters",
            "Give me total Outstanding Loans by Product for Party 'INDI' and Domicile country 'US' for a specific Period 'Dec-2019' for helms": "helms-outstanding_balance",
            "Get me total number of Loans by Product for Party 'INDI' and Domicile country 'US' for a specific Period 'Dec-2019' for hemls": "helms-number_of_loans",
            "Fetch me trend of Outstanding Loans and Number of Loans by Product for Party INDI' and Domicile country 'US' across last four quarters for helms": "helms-trend_over_last_4_quarters",
            "Give me EOP Balance by Product for Party 'CORP' and Domicile country 'US' for a specific Period 'Dec-2019' for HOGAN": "hogan-eop_balance",
            "Get me number of Deposits by Product for Party 'CORP' and Domicile country 'US' for a specific Period 'Dec-2019' for Hogan": "hogan-number_of_deposits",
            "Fetch me trend of total EOP Balance and number of Deposits by Product for Party 'CORP' and Domicile country 'US' across last four quarters for infolease": "hogan-trend_over_last_4_quarters"
        }
        if message in phrases_map:
            return phrases_map.get(message)
        else:
            return False