from bson.objectid import ObjectId

data = {
	"graph": [{
			"_id": ObjectId("5e4a600eb3e62f63dcb0a4c4"),
			"chart_type": "combo",
			"Chart_title": "Balance and Count Trend Chart For HOGAN",
			"x_axis_title": "CORP",
			"y_axis_title": "Metrics",
			"chart_data": [
				[
					"Product_Type",
					"JAN_EOP",
					"DEC_EOP",
					"SEP-EOP",
					"JUN-EOP",
					"MAR-EOP",
					"JAN_NOOFACCN",
					"DEC_NOOFACCN",
					"SEP-NOOFACCN",
					"JUN-NOOFACCN",
					"MAR-NOOFACCN"
				],
				[
					"Checking Account",
					809221124.01,
					712114589.1288,
					1132262196.71479,
					634066830.160284,
					1204726977.30454,
					509,
					448,
					712,
					399,
					758
				],
				[
					"OverDraft Account",
					1281069604.17,
					1127341251.6696,
					1792472590.15466,
					1003784650.48661,
					1907190835.92456,
					1295,
					1140,
					1812,
					1015,
					1928
				],
				[
					"Retirement Account",
					98417972.6,
					86607815.888,
					137706427.26192,
					77115599.2666752,
					146519638.606683,
					610,
					537,
					854,
					478,
					908
				],
				[
					"Saving Account",
					1251956599.92,
					1101721807.9296,
					1751737674.60806,
					980973097.780516,
					1863848885.78298,
					664,
					584,
					929,
					520,
					989
				],
				[
					"Term Deposit Account",
					19052388.7,
					16766102.056,
					26658102.26904,
					14928537.2706624,
					28364220.8142586,
					36,
					32,
					50,
					28,
					54
				]
			],
			"series_type": [
				"bar",
				"bar",
				"bar",
				"bar",
				"bar",
				"line",
				"line",
				"line",
				"line",
				"line"
			],
			"for_metrics": "hogan"
		},
		{
			"_id": ObjectId("5e4a6036b3e62f63dcb0a4dd"),
			"chart_type": "combo",
			"Chart_title": "Balance and Count Trend Chart For HELMS",
			"x_axis_title": "Product Type",
			"y_axis_title": "Metrics",
			"chart_data": [
				[
					"Product_Type",
					"JAN_EOP",
					"DEC_EOP",
					"SEP-EOP",
					"JUN-EOP",
					"MAR-EOP",
					"JAN_NOOFACCN",
					"DEC_NOOFACCN",
					"SEP-NOOFACCN",
					"JUN-NOOFACCN",
					"MAR-NOOFACCN"
				],
				[
					"Student Loan - Bachelors",
					4801814.99,
					4225597.1912,
					6718699.534008,
					3762471.73904448,
					7148696.30418451,
					637,
					561,
					891,
					499,
					948
				],
				[
					"Student Loan - Masters",
					329089.82,
					289599.0416,
					460462.476144,
					257858.98664064,
					489932.074617216,
					40,
					40,
					60,
					30,
					60
				],
				[
					"Student Loan - Others",
					5329578.24,
					4690028.8512,
					7457145.873408,
					4176001.68910848,
					7934403.20930611,
					1000,
					880,
					1399,
					784,
					1489
				]
			],
			"series_type": [
				"bar",
				"bar",
				"bar",
				"bar",
				"bar",
				"line",
				"line",
				"line",
				"line",
				"line"
			],
			"for_metrics": "helms"
		},
		{
			"_id": ObjectId("5e4a6080b3e62f63dcb0a528"),
			"chart_type": "combo",
			"Chart_title": "Balance and Count Trend Chart For Infolease",
			"x_axis_title": "Product Type",
			"y_axis_title": "Metrics",
			"chart_data": [
				[
					"Product_Type",
					"JAN_EOP",
					"DEC_EOP",
					"SEP-EOP",
					"JUN-EOP",
					"MAR-EOP",
					"JAN_NOOFACCN",
					"DEC_NOOFACCN",
					"SEP-NOOFACCN",
					"JUN-NOOFACCN",
					"MAR-NOOFACCN"
				],
				[
					"Lease Purchase",
					8162064.49,
					7182616.7512,
					11420360.634408,
					6395401.95526848,
					12151263.7150101,
					1097,
					965,
					1534,
					859,
					1632
				],
				[
					"Operating Lease",
					2173777.07,
					1912923.8216,
					3041548.876344,
					1703267.37075264,
					3236208.00443002,
					350,
					308,
					489,
					273,
					518
				],
				[
					"Rental Lease",
					325641.72,
					286564.7136,
					455637.894624,
					255157.22098944,
					484798.719879936,
					200,
					176,
					279,
					156,
					296
				]
			],
			"series_type": [
				"bar",
				"bar",
				"bar",
				"bar",
				"bar",
				"line",
				"line",
				"line",
				"line",
				"line"
			],
			"for_metrics": "infolease"
		},
		{
			"_id": ObjectId("5e4ae9d4b3e62f63dcb10c93"),
			"chart_type": "basic_line",
			"Chart_title": "No Of Accounts Chart For CORP",
			"x_axis_title": "CORP",
			"y_axis_title": "Sum Of Number Of Accounts",
			"chart_data": [
				[
					"Product Type",
					"NO OF ACCOUNTS"
				],
				[
					"Checking Account",
					509
				],
				[
					"OverDraft Account",
					1295
				],
				[
					"Retirement Account",
					610
				],
				[
					"Saving Account",
					664
				],
				[
					"Term Deposit Account",
					36
				]
			],
			"for_metrics": "hogan-number_of_deposits"
		},
		{
			"_id": ObjectId("5e4ae9e6b3e62f63dcb10c9d"),
			"chart_type": "basic_bar",
			"Chart_title": "Total Balance Chart For CORP",
			"x_axis_title": "CORP",
			"y_axis_title": "Sum Of EOP Balance",
			"chart_data": [
				[
					"Product Type",
					"EOP BALANCE"
				],
				[
					"Checking Account",
					80922112.401
				],
				[
					"OverDraft Account",
					128106960.417
				],
				[
					"Retirement Account",
					9841797.26
				],
				[
					"Saving Account",
					125195659.992
				],
				[
					"Term Deposit Account",
					1905238.87
				]
			],
			"for_metrics": "hogan-eop_balance"
		},
		{
			"_id": ObjectId("5e4ae9f9b3e62f63dcb10ca2"),
			"chart_type": "basic_line",
			"Chart_title": "No Of Accounts Chart For HELMS",
			"x_axis_title": "Product Type",
			"y_axis_title": "Sum Of Number Of Accounts",
			"chart_data": [
				[
					"Product Type",
					"NO OF ACCOUNTS"
				],
				[
					"Student Loan - Bachelors",
					637
				],
				[
					"Student Loan - Masters",
					49
				],
				[
					"Student Loan - Others",
					1000
				]
			],
			"for_metrics": "helms-number_of_loans"
		},
		{
			"_id": ObjectId("5e4aea08b3e62f63dcb10caa"),
			"chart_type": "basic_line",
			"Chart_title": "No Of Accounts Chart For Infolease",
			"x_axis_title": "Product Type",
			"y_axis_title": "Sum Of Number Of Accounts",
			"chart_data": [
				[
					"Product Type",
					"NO OF ACCOUNTS"
				],
				[
					"Lease Purchase",
					1097
				],
				[
					"Operating Lease",
					350
				],
				[
					"Rental Lease",
					200
				]
			],
			"for_metrics": "infolease-number_of_loans"
		},
		{
			"_id": ObjectId("5e4aea18b3e62f63dcb10cb6"),
			"chart_type": "basic_bar",
			"Chart_title": "Total Balance Chart For HELMS",
			"x_axis_title": "Product Type",
			"y_axis_title": "Sum Of EOP Balance",
			"chart_data": [
				[
					"Product Type",
					"EOP BALANCE"
				],
				[
					"Student Loan - Bachelors",
					4801814.99
				],
				[
					"Student Loan - Masters",
					329089.82
				],
				[
					"Student Loan - Others",
					5329578.24
				]
			],
			"for_metrics": "helms-outstanding_balance"
		},
		{
			"_id": ObjectId("5e4aea4eb3e62f63dcb10cd0"),
			"chart_type": "basic_bar",
			"Chart_title": "Total Balance Chart For Infolease",
			"x_axis_title": "Product Type",
			"y_axis_title": "Sum Of EOP Balance",
			"chart_data": [
				[
					"Product Type",
					"EOP BALANCE"
				],
				[
					"Lease Purchase",
					8162064.49
				],
				[
					"Operating Lease",
					2173777.07
				],
				[
					"Rental Lease",
					325641.72
				]
			],
			"for_metrics": "infolease-outstanding_balance"
		},
		{
			"_id": ObjectId("5e4aea60b3e62f63dcb10cda"),
			"chart_type": "combo",
			"Chart_title": "Balance and Count Trend Chart For HOGAN",
			"x_axis_title": "CORP",
			"y_axis_title": "Metrics",
			"chart_data": [
				[
					"Product Type",
					"JAN EOP",
					"DEC EOP",
					"SEP EOP",
					"JUN EOP",
					"MAR EOP",
					"JAN NOOFACCN",
					"DEC NOOFACCN",
					"SEP NOOFACCN",
					"JUN NOOFACCN",
					"MAR NOOFACCN"
				],
				[
					"Checking Account",
					809221124.01,
					712114589.1288,
					1132262196.71479,
					634066830.160284,
					1204726977.30454,
					509,
					448,
					712,
					399,
					758
				],
				[
					"OverDraft Account",
					1281069604.17,
					1127341251.6696,
					1792472590.15466,
					1003784650.48661,
					1907190835.92456,
					1295,
					1140,
					1812,
					1015,
					1928
				],
				[
					"Retirement Account",
					98417972.6,
					86607815.888,
					137706427.26192,
					77115599.2666752,
					146519638.606683,
					610,
					537,
					854,
					478,
					908
				],
				[
					"Saving Account",
					1251956599.92,
					1101721807.9296,
					1751737674.60806,
					980973097.780516,
					1863848885.78298,
					664,
					584,
					929,
					520,
					989
				],
				[
					"Term Deposit Account",
					19052388.7,
					16766102.056,
					26658102.26904,
					14928537.2706624,
					28364220.8142586,
					36,
					32,
					50,
					28,
					54
				]
			],
			"series_type": [
				"bar",
				"bar",
				"bar",
				"bar",
				"bar",
				"line",
				"line",
				"line",
				"line",
				"line"
			],
			"for_metrics": "hogan-trend_over_last_4_quarters"
		},
		{
			"_id": ObjectId("5e4aea6db3e62f63dcb10cdf"),
			"chart_type": "combo",
			"Chart_title": "Balance and Count Trend Chart For HELMS",
			"x_axis_title": "Product Type",
			"y_axis_title": "Metrics",
			"chart_data": [
				[
					"Product Type",
					"JAN EOP",
					"DEC EOP",
					"SEP EOP",
					"JUN EOP",
					"MAR EOP",
					"JAN NOOFACCN",
					"DEC NOOFACCN",
					"SEP NOOFACCN",
					"JUN NOOFACCN",
					"MAR NOOFACCN"
				],
				[
					"Student Loan - Bachelors",
					4801814.99,
					4225597.1912,
					6718699.534008,
					3762471.73904448,
					7148696.30418451,
					637,
					561,
					891,
					499,
					948
				],
				[
					"Student Loan - Masters",
					329089.82,
					289599.0416,
					460462.476144,
					257858.98664064,
					489932.074617216,
					40,
					40,
					60,
					30,
					60
				],
				[
					"Student Loan - Others",
					5329578.24,
					4690028.8512,
					7457145.873408,
					4176001.68910848,
					7934403.20930611,
					1000,
					880,
					1399,
					784,
					1489
				]
			],
			"series_type": [
				"bar",
				"bar",
				"bar",
				"bar",
				"bar",
				"line",
				"line",
				"line",
				"line",
				"line"
			],
			"for_metrics": "helms-trend_over_last_4_quarters"
		},
		{
			"_id": ObjectId("5e4aea7eb3e62f63dcb10ce7"),
			"chart_type": "combo",
			"Chart_title": "Balance and Count Trend Chart For Infolease",
			"x_axis_title": "Product Type",
			"y_axis_title": "Metrics",
			"chart_data": [
				[
					"Product Type",
					"JAN EOP",
					"DEC EOP",
					"SEP EOP",
					"JUN EOP",
					"MAR EOP",
					"JAN NOOFACCN",
					"DEC NOOFACCN",
					"SEP NOOFACCN",
					"JUN NOOFACCN",
					"MAR NOOFACCN"
				],
				[
					"Lease Purchase",
					8162064.49,
					7182616.7512,
					11420360.634408,
					6395401.95526848,
					12151263.7150101,
					1097,
					965,
					1534,
					859,
					1632
				],
				[
					"Operating Lease",
					2173777.07,
					1912923.8216,
					3041548.876344,
					1703267.37075264,
					3236208.00443002,
					350,
					308,
					489,
					273,
					518
				],
				[
					"Rental Lease",
					325641.72,
					286564.7136,
					455637.894624,
					255157.22098944,
					484798.719879936,
					200,
					176,
					279,
					156,
					296
				]
			],
			"series_type": [
				"bar",
				"bar",
				"bar",
				"bar",
				"bar",
				"line",
				"line",
				"line",
				"line",
				"line"
			],
			"for_metrics": "infolease-trend_over_last_4_quarters"
		}
	],
	"bot_dto": [
		{
			"_id" : ObjectId("5e46703f0a4f0743942bc13e"),
			"created_by" : "Superuser",
			"bot_name" : "Chatbot",
			"bot_desc" : "",
			"es_analyzer" : "false",
			"bot_language" : "en",
			"small_talk_id" : "5e4145cee22fbea089033349",
			"ae_id" : "",
			"survey_id" : "",
			"survey_status" : "false",
			"esc_id" : "",
			"non_cct_id" : "",
			"ui_id" : "5e46703f0a4f0743942bc13d",
			"chatflow" : "5e4670450a4f0743942bc2b4",
			"botgroup_id" : "5e257a9c45116cb60c450466",
			"is_avatar_set" : "false",
			"subflow" : [],
			"modified_by" : "Superuser"
		}
	],
	"chatflow_dto": [
		{
		"_id" : ObjectId("5e4670450a4f0743942bc2b4"),
		"created_by" : "Superuser",
		"name" : "main flow",
		"description" : "",
		"chatflow_type" : "CCT",
		"language" : "",
		"mapping" : [ 
		{
		"id" : ObjectId("5e4670eb0a4f0743942bc2b9"),
		"task_name" : "Hello, Welcome to the GC Assist. Please select one from below options",
		"task_text" : [ 
		    "Hello, Welcome to the GC Assist.Please Choose the portfolio to proceed.\n"
		],
		"answers" : "",
		"is_reserved" : "false",
		"is_start" : "true",
		"interaction_type" : "button",
		"interaction_values" : [ 
		    "Credit portfolio", 
		    "Deposit portfolio"
		],
		"next_task_ids" : [ 
		    "2", 
		    "3"
		],
		"interaction_db_filter_value" : "",
		"action_type" : "navigate",
		"function_code" : "",
		"interaction_db_select_column" : "",
		"interaction_fetch_from_db" : "false",
		"interaction_db_filter_column" : "",
		"keywords" : [ 
		    ""
		],
		"alias" : "false",
		"interaction_db_host" : "",
		"bot_name" : "",
		"interaction_db_port" : "",
		"interaction_db_table_name" : "",
		"interaction_db_type" : "",
		"interaction_connection_string" : "",
		"task_id" : 1,
		"use_code" : "",
		"subflow_id" : "",
		"subflow_type" : ""
		}, 
		{
		"id" : ObjectId("5e4674c40a4f0743942bc48d"),
		"task_name" : "Credit portfolio",
		"task_text" : [ 
		    "Please select one of the following option."
		],
		"answers" : "",
		"is_reserved" : "false",
		"is_start" : "false",
		"interaction_type" : "button",
		"interaction_values" : [ 
		    "INFOLEASE", 
		    "HELMS", 
		    "Restart"
		],
		"next_task_ids" : [ 
		    "4", 
		    "5", 
		    "1"
		],
		"interaction_db_filter_value" : "",
		"action_type" : "navigate",
		"function_code" : "",
		"interaction_db_select_column" : "",
		"interaction_fetch_from_db" : "false",
		"interaction_db_filter_column" : "",
		"keywords" : [ 
		    ""
		],
		"alias" : "false",
		"interaction_db_host" : "",
		"bot_name" : "",
		"interaction_db_port" : "",
		"interaction_db_table_name" : "",
		"interaction_db_type" : "",
		"interaction_connection_string" : "",
		"task_id" : 2,
		"use_code" : "",
		"subflow_id" : "",
		"subflow_type" : ""
		}, 
		{
		"id" : ObjectId("5e4675000a4f0743942bc4a0"),
		"task_name" : "Deposit portfolio",
		"task_text" : [ 
		    "Please choose one from following options."
		],
		"answers" : "",
		"is_reserved" : "false",
		"is_start" : "false",
		"interaction_type" : "button",
		"interaction_values" : [ 
		    "HOGAN", 
		    "Restart"
		],
		"next_task_ids" : [ 
		    "6", 
		    "1"
		],
		"interaction_db_filter_value" : "",
		"action_type" : "navigate",
		"function_code" : "",
		"interaction_db_select_column" : "",
		"interaction_fetch_from_db" : "false",
		"interaction_db_filter_column" : "",
		"keywords" : [ 
		    ""
		],
		"alias" : "false",
		"interaction_db_host" : "",
		"bot_name" : "",
		"interaction_db_port" : "",
		"interaction_db_table_name" : "",
		"interaction_db_type" : "",
		"interaction_connection_string" : "",
		"task_id" : 3,
		"use_code" : "",
		"subflow_id" : "",
		"subflow_type" : ""
		}, 
		{
		"id" : ObjectId("5e46763f0a4f0743942bc62d"),
		"task_name" : "INFOLEASE",
		"task_text" : [ 
		    "Please select Metrics"
		],
		"answers" : "",
		"is_reserved" : "false",
		"is_start" : "false",
		"interaction_type" : "button",
		"interaction_values" : [ 
		    "Outstanding Balance", 
		    "Number of loans", 
		    "Trend over last 4 quarters", 
		    "Restart"
		],
		"next_task_ids" : [ 
		    "7", 
		    "7", 
		    "1"
		],
		"interaction_db_filter_value" : "",
		"action_type" : "navigate",
		"function_code" : "",
		"interaction_db_select_column" : "",
		"interaction_fetch_from_db" : "false",
		"interaction_db_filter_column" : "",
		"keywords" : [ 
		    ""
		],
		"alias" : "false",
		"interaction_db_host" : "",
		"bot_name" : "",
		"interaction_db_port" : "",
		"interaction_db_table_name" : "",
		"interaction_db_type" : "",
		"interaction_connection_string" : "",
		"task_id" : 4,
		"use_code" : "",
		"subflow_id" : "",
		"subflow_type" : ""
		}, 
		{
		"id" : ObjectId("5e46805a0a4f0743942beafd"),
		"task_name" : "HELMS",
		"task_text" : [ 
		    "Please select Metrics"
		],
		"answers" : "",
		"is_reserved" : "false",
		"is_start" : "false",
		"interaction_type" : "button",
		"interaction_values" : [ 
		    "Outstanding Balance", 
		    "Number of loans", 
		    "Trend over last 4 quarters", 
		    "Restart"
		],
		"next_task_ids" : [ 
		    "7", 
		    "7", 
		    "1"
		],
		"interaction_db_filter_value" : "",
		"action_type" : "navigate",
		"function_code" : "",
		"interaction_db_select_column" : "",
		"interaction_fetch_from_db" : "false",
		"interaction_db_filter_column" : "",
		"keywords" : [ 
		    ""
		],
		"alias" : "false",
		"interaction_db_host" : "",
		"bot_name" : "",
		"interaction_db_port" : "",
		"interaction_db_table_name" : "",
		"interaction_db_type" : "",
		"interaction_connection_string" : "",
		"task_id" : 5,
		"use_code" : "",
		"subflow_id" : "",
		"subflow_type" : ""
		}, 
		{
		"id" : ObjectId("5e4681790a4f0743942bee3b"),
		"task_name" : "HOGAN",
		"task_text" : [ 
		    "Please select Metrics"
		],
		"answers" : "",
		"is_reserved" : "false",
		"is_start" : "false",
		"interaction_type" : "button",
		"interaction_values" : [ 
		    "EOP balance", 
		    "Number of deposits", 
		    "Trend over last 4 quarters", 
		    "Restart"
		],
		"next_task_ids" : [ 
		    "7", 
		    "7", 
		    "1"
		],
		"interaction_db_filter_value" : "",
		"action_type" : "navigate",
		"function_code" : "",
		"interaction_db_select_column" : "",
		"interaction_fetch_from_db" : "false",
		"interaction_db_filter_column" : "",
		"keywords" : [ 
		    ""
		],
		"alias" : "false",
		"interaction_db_host" : "",
		"bot_name" : "",
		"interaction_db_port" : "",
		"interaction_db_table_name" : "",
		"interaction_db_type" : "",
		"interaction_connection_string" : "",
		"task_id" : 6,
		"use_code" : "",
		"subflow_id" : "",
		"subflow_type" : ""
		}, 
		{
		"id" : ObjectId("5e4800360a4f07776c70e6e9"),
		"task_name" : "Outstanding Balance;Number of loans;EOP balance;Number of deposits; Trend over last 4 quarters;Show Filters",
		"task_text" : [ 
		    "Please select filter criteria"
		],
		"answers" : "",
		"is_reserved" : "false",
		"is_start" : "false",
		"interaction_type" : "button",
		"interaction_values" : [ 
		    "Product", 
		    "Party Type", 
		    "Domicile Country", 
		    "Period", 
		    "Show Results", 
		    "Restart"
		],
		"next_task_ids" : [ 
		    "7"
		],
		"interaction_db_filter_value" : "",
		"action_type" : "navigate",
		"function_code" : "",
		"interaction_db_select_column" : "",
		"interaction_fetch_from_db" : "false",
		"interaction_db_filter_column" : "",
		"keywords" : [ 
		    ""
		],
		"alias" : "false",
		"interaction_db_host" : "",
		"bot_name" : "",
		"interaction_db_port" : "",
		"interaction_db_table_name" : "",
		"interaction_db_type" : "",
		"interaction_connection_string" : "",
		"task_id" : 7,
		"use_code" : "",
		"subflow_id" : "",
		"subflow_type" : ""
		}
		],
		"modified_by" : "Superuser"
		}
	]
}