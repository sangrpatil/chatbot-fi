from utility.mongo_dao import insert, get_by_id, get_one, update, get_collection, get_all
import datetime
from utility.logger import logger
from bson.objectid import ObjectId
import xlsxwriter
import json
from collections import OrderedDict

class EscalateData:
    def create_report(session_id):
        try:
            data = get_all("interactions", "session_id", session_id)
            data_to_write = []
            for element in data:
                data_to_write.append(eval(element))
            for element in data_to_write:
                for k, v in element.items():
                    element[k] = str(v)
            return EscalateData.create_excel(data_to_write)
        except Exception as e:
            logger.error(str(e))
            print(str(e))
            return "Error Occurred" + str(e)

    @staticmethod
    def json_to_excel(ws, workbook, data_in_list):
        header = ['message', "comments", 'type', "adverse_event", 'log_time', 'FuzzySearch', 'ASR', 'endSurveyFlag', 'restartGuidedIndex', 'bot_name', 'date_created', 'has_ended', 'current_state', 'session_id',"rating"]
        formatting = workbook.add_format({
            'size': 15,
            'bold': 4,
            'border': 4,
            'align': 'center',
            'valign': 'vcenter',
            'font_color': 'black',
            'fg_color': '#007DC6'})
        sudo_header = []
        for element in header:
            sudo_header.append(element.lower().replace(' ', '_'))
        ws.set_column(0, 15, 25)
        ws.write_row('A1', header, cell_format=formatting)
        row = 1
        for i in data_in_list:
            for k, v in i.items():
                if k == "_id":
                    continue
                elif k == "bot_id":
                    k = "bot_name"
                    v = get_by_id("bot_dto", v)["bot_name"]
                    idx = sudo_header.index(k.lower().replace(' ', '_'))
                    ws.write(row, idx, str(v))
                elif not isinstance(v, str):
                    v = str(v)
                    idx = sudo_header.index(k.lower().replace(' ', '_'))
                    ws.write(row, idx, str(v))
                else:
                    try:
                        idx = sudo_header.index(k.lower().replace(' ', '_'))
                        ws.write(row, idx, str(v))
                    except:
                        continue
            row+=1

    def create_excel(data):
        import io
        data_to_write = json.loads(json.dumps(data), object_pairs_hook=OrderedDict)
        output = io.BytesIO()
        wb = xlsxwriter.workbook.Workbook(output, {'in_memory': True})
        ws = wb.add_worksheet('Escalation Sheet')
        EscalateData.json_to_excel(ws,wb, data_to_write)
        wb.close()
        output.seek(0)
        return output


