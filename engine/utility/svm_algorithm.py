import os
import pickle
import sys

import numpy as np
import pandas as pd
from utility.logger import logger
from sklearn import model_selection, svm
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.preprocessing import LabelEncoder


class SVMAlgorithm:
    """Train the SVM model and use it for predict the Question Category"""

    def __init__(self):
        self.load_pickle_files()
        self.svm_model = pickle.load(open("svm_obj.pkl", "rb"))
        self.tfidf_vect = pickle.load(open("tfidf_vect.pkl", "rb"))
        self.encoder = pickle.load(open("encoder.pkl", "rb"))

    def load_pickle_files(self):
        # check pickle files, if not present then create new
        encoder_file = os.path.exists("encoder.pkl")
        tfidf_vect_file = os.path.exists("tfidf_vect.pkl")
        svm_file = os.path.exists("svm_obj.pkl")
        if not encoder_file or not tfidf_vect_file or not svm_file:
            self.train_the_model()

    def train_the_model(self):
        try:
            corpus = self.get_corpus_data()

            # split the corpus into training and test data (train:test = 7:3)
            train_x, test_x, train_y, test_y = model_selection.train_test_split(
                corpus['Questions'], corpus['Category'], test_size=0.3)

            # Encoding the label array to numerals and vectorizing the data using TFIDF
            encoder = LabelEncoder()
            train_y = encoder.fit_transform(train_y)
            test_y = encoder.fit_transform(test_y)
            tfidf_vect = TfidfVectorizer(max_features=5000)
            tfidf_vect.fit(corpus['Questions'])
            train_x_tfidf = tfidf_vect.transform(train_x)
            test_x_tfidf = tfidf_vect.transform(test_x)

            # applying SVM alogirthm
            svm_obj = svm.SVC(C=1.0, kernel='linear', degree=3, gamma='auto')
            svm_obj.fit(train_x_tfidf, train_y)

            # store the data into pick
            pickle.dump(svm_obj, open("svm_obj.pkl", 'wb'))
            pickle.dump(tfidf_vect, open("tfidf_vect.pkl", 'wb'))
            pickle.dump(encoder, open("encoder.pkl", 'wb'))

        except Exception as e:
            logger.error("SVM algorithm Failed: {}".format(str(e)))
            return False

    @staticmethod
    def get_corpus_data():
        # return corpus data
        # TODO: add it in environment variable
        try:
            __location__ = os.path.dirname(os.path.abspath(__file__))
            file_path = os.path.join(__location__, 'faq_corpus.xlsx')
            corpus = pd.read_excel(file_path, encoding='utf-8')
            return corpus
        except Exception as e:
            logger.error("Unable to find corpus for SVM : {}".format(str(e)))
            return False

    def predict_question_category(self, message):
        # predict category for given message/question
        try:
            question_vect = self.tfidf_vect.transform([message])
            prediction = np.array(self.encoder.inverse_transform(self.svm_model.predict(question_vect)))
            return prediction[0] if len(prediction) else ""
        except Exception as e:
            logger.error("Unable to predict question Category: {}".format(str(e)))
            return ""

    def test_prediction(self):
        # for testing
        question_list = ["how far it is?", "Where exactly?"]
        question_vect = self.tfidf_vect.transform(question_list)
        prediction = np.array(self.encoder.inverse_transform(self.svm_model.predict(question_vect)))
        print("Test prediction result .....", prediction)
        return prediction
