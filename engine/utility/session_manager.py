# Sets and gets context of an user
from utility.mongo_dao import insert, get_by_id, update
from utility.logger import logger



def set(context_json):
    # returns context_id
    try:
        context_id = insert('context', context_json)
        return context_id
    except Exception as e:
        logger.error("Session manager error: ", str(e))

def modify(context_id, context_json):
    try:
        context_id = update('context', context_id, context_json)
        return context_id
    except Exception as e:
        logger.error("Session manager error: ", str(e))

def get(context_id):
    # returns context_json
    try:
        context_json = get_by_id('context', context_id)
        return context_json
    except Exception as e:
        logger.error("Session manager error: ", str(e))


def set_chat_data(context_json):
    try:
        context_id = insert('chat_data', context_json)
        return context_id
    except Exception as e:
        logger.error("Session manager error: ", str(e))

def set_chat_stat_faq(context_json):
    try:
        context_id = insert('chat_stat_faq', context_json)
        return context_id
    except Exception as e:
        logger.error("Session manager error: ", str(e))