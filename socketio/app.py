# Generic package imports
import json
import logging
import os
import uuid
import wave
import datetime
import io

import requests
from flask import Flask, request, session, current_app, jsonify, send_file
from flask_babel import gettext
from flask_socketio import emit, SocketIO
from utility.mongo_dao import get_by_id, get_one
from bson.objectid import ObjectId
from flask_cors import CORS

app = Flask(__name__)
app.config['SECRET_KEY'] = 'D3L0iTT3'
app.config['FILEDIR'] = 'static/_files/'
socketio = SocketIO(app, cors_allowed_origins="*")
CORS(app)
from utility.ASR import transcribe_file

# Set the logging
logger = logging.getLogger()
logger.setLevel(logging.DEBUG)

# HOST AND PORT SETTINGS
server_host = os.environ.get('SERVER_HOST') or 'localhost'
server_port = os.environ.get('SERVER_PORT') or '8000'


# #ID's
# session['g_session_id'] = ''
# session['g_bot_id'] = ''

# Validate the id's here
# todo there could be a scenario where one thread completes faster than another. must force to be synchronous
def validate_id(type, id):
    return 0, 'Success'
    post_request_url = 'http://' + server_host + ':' + str(server_port) + '/validate/' + type + '/' + id
    response = requests.get(post_request_url)
    response_json = json.loads(response.text)
    response_status = response_json['result']
    if response_status == 1:
        return 1, response_json['msg']
    else:
        return 0, response_json['msg']
    pass


@socketio.on('disconnect')
def HandleDisconnect():
    print('---DISCONNECTING---')
    handleEnd({"bot_id": session['g_bot_id'], "session_id": session['g_session_id']})


@socketio.on('start-recording')
def start_recording(options):
    # print('START RECORDING, SESSION ID - ', session_id)
    """Start recording audio from the client."""
    # session = get_context(session_id)
    id = uuid.uuid4().hex  # server-side filename
    session['wavename'] = id + '.wav'
    wf = wave.open(current_app.config['FILEDIR'] + session['wavename'], 'wb')
    wf.setnchannels(options.get('numChannels', 1))
    wf.setsampwidth(options.get('bps', 16) // 8)
    wf.setframerate(options.get('fps', 44100))
    session['wavefile'] = wf  # todo try storing in mongoDB
    # update_context(session_id, session)


@socketio.on('write-audio')
def write_audio(data):
    # print('WRITING AUDIO, SESSION ID - ', session_id)
    """Write a chunk of audio from the client."""
    # session = get_context(session_id)
    session['wavefile'].writeframes(data)
    # update_context(session_id, session)


@socketio.on('end-recording')
def end_recording(data):
    # print('END AUDIO, SESSION ID - ', session_id)
    # session = get_context(session_id)
    """Stop recording audio from the client."""
    # print('GOING TO CALL GOOGLE S2T API')
    statinfo = os.stat(current_app.config['FILEDIR'] + session['wavename'])
    if statinfo.st_size == 0:
        return jsonify(
            # {"message":"Ask a question", "state":""}
            message=gettext('Sorry, I could not get your audio input. Could you try again?'),
            state='',
            ASR='True'
        )
    transcript_text = transcribe_file(current_app.config['FILEDIR'] + session['wavename'])
    # print(transcript_text)
    if str(transcript_text) != 'None':
        emit('add-wavefile', transcript_text)
    session['wavefile'].close()
    # delete the file
    try:
        pass
        os.remove(current_app.config['FILEDIR'] + session['wavename'])
    except Exception as e:
        print(e)
    # del session['wavefile']
    # del session['wavename']
    # GOING TO SEND THE TRANSCRIPTED MESSAGE TO ELASTICSEARCH

    # execution_engine = ExecutionEngine(session['session_id'])
    # update_context(session_id,session)

    # bot_id = str(request.args.get('bot_id'))
    bot_id = data['bot_id']
    session_id = data['session_id']
    print('bot id and session id ', data['bot_id'], data['session_id'])
    # Validate Bot_id - POST request
    response_status, message = validate_id('bot', bot_id)
    if response_status == 1:
        logger.error(message)
        print(message)
    else:
        # Get the session id from UI parameter
        # Validate session_id - POST request
        response_status, message = validate_id('session', session_id)

        # if session_id is invalid then get new session_id
        if response_status == 1:
            logger.info('Original session from UI cookie expired - ' + message)
            get_request_url = 'http://' + server_host + ':' + str(server_port) + '/bot/' + bot_id + '/init'
            res = request.get(get_request_url)
            response = json.loads(res.text)
            print("Init RESPONSE", response)

            # Initialize session_id
            if str(response['status']) == 'Success':
                print('SESSION ID SUCCESS')
                session_id = str(response['session_id'])
                emit('session', session_id)
            else:
                logger.info(message)
                print('SESSION ID FAILURE - ', message)
            if str(transcript_text) != 'None':
                print('Message sent as ASR text')
                handleMessage({"message": transcript_text, 'session_id': session_id, 'bot_id': bot_id, 'ASRFlag': True})
            else:
                handleMessage({"message": None, 'session_id': session_id, 'bot_id': bot_id, 'ASRFlag': True})
        else:
            if str(transcript_text) != 'None':
                print('Message sent as ASR text')
                handleMessage({"message": transcript_text, 'session_id': session_id, 'bot_id': bot_id, 'ASRFlag': True})
            else:
                handleMessage({"message": None, 'session_id': session_id, 'bot_id': bot_id, 'ASRFlag': True})


@socketio.on('end')
def handleEnd(params):
    # url = "http://localhost:8000/bot/5b106f08e0923321209e65bd/5b184a22e092332560db492b/end"
    print('END HERE')
    # bot_id = str(request.args.get('bot_id'))
    bot_id = params['bot_id']
    # Validate Bot_id - POST request
    response_status, message = validate_id('bot', bot_id)
    if response_status == 1:
        logger.error(message)
        print(message)
    else:
        # Get the session id from UI parameter
        # session_id = str(request.args.get('session_id'))
        # print('--PARAMS--', params)
        session_id = params['session_id']
        print('---CHECKING SESSION ID---', session_id)
        # Validate session_id - POST request
        # response_status, message = validate_id('session', session_id)

        # if session_id is invalid then get new session_id
        if response_status == 1:
            logger.info('Original session from UI cookie expired - ' + message)
        else:

            url = 'http://' + server_host + ':' + str(server_port) + '/bot/' + bot_id + '/' + session_id + '/end'
            response = requests.request("POST", url)
            response_json = json.loads(response.text)
            print('--END MSG RESPONSE JSON - ', response_json['msg'])
            # logger.log(response_json['result']['text'])


@socketio.on('connect')
def handleConnect():
    # Get the bot_id
    print('in handle connect', request.args)
    bot_id = str(request.args.get('bot_id'))
    # bot_id = '5b169827e0923312ccbd27f3'
    print(bot_id, 'bot_id')
    # Validate Bot_id - POST request
    # response_status, message = validate_id('bot',bot_id)
    response_status, message = 0, "Success"
    if response_status == 1:
        logger.error(message)
        print('for bot id ', bot_id, ' ', message)
    else:
        # Get the session id from UI parameter
        session_id = str(request.args.get('session_id'))
        print('--CHECKING SESSION ID CONNECT BEFORE VALIDATE--', session_id)
        # Validate session_id - POST request
        # response_status, message = validate_id('session',session_id)
        response_status, message = 1, "success"

        # if session_id is invalid then get new session_id
        if response_status == 1:
            logger.info('Original session from UI cookie expired - ' + message)
            print('--SESSION WAS EXPIRED---')
            # Send http GET to initialize session
            get_request_url = 'http://' + server_host + ':' + str(server_port) + '/bot/' + bot_id + '/init'
            print("Get_reuested_url", get_request_url)
            response = json.loads(requests.get(get_request_url).text)
            # Initialize session_id
            if str(response['status']) == 'Success':
                print('SESSION ID SUCCESS')
                session_id = str(response['session_id'])
                emit('session', session_id)
            else:
                logger.info(message)
                print('SESSION ID FAILURE - ', message)
            print('--CHECKING SESSION ID IF IT WAS INVALID--', session_id)

            post_request_url = 'http://' + server_host + ':' + str(
                server_port) + '/bot/' + bot_id + '/' + session_id + '/chat'
            post_request_data = json.dumps({"message": "", "state": "", "ASR": False})
            session['g_bot_id'] = bot_id
            session['g_session_id'] = session_id
            # sending post request and saving response as response object
            # response = requests.post(url=post_request_url, data=post_request_data)
            headers = {'content-type': "application/json"}
            print("Chat URL, requst data", post_request_url, post_request_data)
            response = requests.request("POST", post_request_url, data=post_request_data, headers=headers)
            response_json = json.loads(response.text)
            print("response_json", response)

            # print('MSG RESPONSE JSON - ',response_json['result']['message'][0])
            try:
                if response_json['status'] == 'Success':
                    if not response_json['result']['is_multi']:
                        emit('interaction_message', response_json['result']['message'][0])  # [0]['text'])
                        # if response_json['result']['message'][0]['tts'] == True:
                        #     print('EMITING TTS')
                        #     emit('audioTTSOutput', response_json['result']['message'][0]['tts audio'])
                    else:
                        for message in response_json['result']['message']:
                            emit('interaction_message', message)
                            # if message['tts'] == True:
                            #     emit('audioTTSOutput', message['tts audio'])
                            # emit('audioTTSOutput',)
            except Exception as e:
                logger.error(e)
        else:
            print('--CHECKING SESSION ID CONNECT--', session_id)
            handleMessage({'message': '', 'session_id': session_id, 'bot_id': bot_id, 'ASRFlag': False})
    pass


@socketio.on('message')
def handleMessage(msg):
    print('IN HANDLE MESSAGE, and the msg is ', msg)
    # Validate Bot_id - POST request
    # msg['bot_id'] = '5b861fd345a8993f7d6fd921'

    # FI work-around
    if isinstance(msg["message"], dict):
        msg["message"] = msg["message"]["text"]

    response_status, message = validate_id('bot', msg['bot_id'])
    # asr_flag = msg['ASRFlag']
    if 'ASRFLag' in msg.keys():
        asr_flag = msg['ASRFlag']
    else:
        asr_flag = 0
    if response_status == 1:
        logger.error(message)
        print('for bot id ', msg['bot_id'], ' ', message)
    else:
        # Validate session_id - POST request
        # response_status, message = validate_id('session',msg['session_id'])
        print('in the handle message bot id is valid')
        post_request_url = 'http://' + server_host + ':' + str(server_port) + '/bot/' + msg['bot_id'] + '/' + msg[
            'session_id'] + '/chat'
        session['g_bot_id'] = msg['bot_id']
        session['g_session_id'] = msg['session_id']
        print('POST REQUEST URL - ', post_request_url)
        headers = {'content-type': "application/json"}
        # if response_status == 1:
        #     logger.error(message)
        #     print('SESSION ID IS INVALID _ ',message)
        #     post_request_data = json.dumps({"message":'', "state": "", "ASR": "false"})
        #     r = requests.request("POST", post_request_url, data=post_request_data, headers=headers) #nothing to emit, just re-execute the current state?

        # else:
        # sending post request and saving response as response object
        post_request_data = json.dumps({"message": msg['message'], "state": "", "ASR": asr_flag})
        r = requests.request("POST", post_request_url, data=post_request_data, headers=headers)
        r_json = json.loads(r.text)
        print('R JSON', r_json)
        # print('THE MESSAGE JSON - ',r_json)
        if r_json['status'] == 'Success':
            try:
                if r_json['result']['is_multi'] == False:
                    data = r_json['result']['message'][0]
                    print('THE HANDLE MESSAGE TO PRINT IS ', r_json['result']['message'][0])
                    emit('interaction_message', data)  # [0]['text'])
                    # if r_json['result']['message'][0]['tts'] == True:
                    #     emit('audioTTSOutput', r_json['result']['message'][0]['tts audio'])
                else:
                    for message in r_json['result']['message']:
                        emit('interaction_message', message)
                        # if message['tts'] == True:
                        #     emit('audioTTSOutput', message['tts audio'])
                        # print('MSSG IS - ', message)

            except Exception as e:
                print('HANDLE MESSAGE ERROR IS ', e)
                emit('message', r_json['result'])
                logger.error(e)

@app.route('/webconsole-theame/<bot_id>')
def get_webconsole_theame(bot_id):
    try:
        bot = get_by_id('bot_dto', bot_id)
        if not bot:
            return jsonify(status='Failure', msg='Invalid Bot ID'), 404
        if not bot.get("ui_id"):
            return jsonify(status='Failure', msg='Web Console Theame Not Found'), 404

        web_console = get_by_id('ui_theme_dto', bot.get("ui_id"))
        if web_console:
            data = eval(str(web_console))
            data["id"] = str(data.pop("_id"))
            data["is_avatar_set"] = bot.get("is_avatar_set", "false")
            return jsonify(status="Success", data=data), 200
        else:
            return jsonify(status='Failure', msg='Invalid Web Console Theame ID'), 404
    except Exception as e:
        logger.error("Failed to fetch Bot Details: {}".format(str(e)))
        return jsonify(status='Failure', msg='Web Console Theame Not Found'), 404


@app.route('/avatar/<avtar_type>/<bot_id>')
def get_avatar(avtar_type, bot_id):
    try:
        # return given type of avatar from respective collection
        avatar_collection = {
            "bot-avatar": "avatar_dto",
            "user-avatar": "user_avatar_dto",
            "header-avatar": "header_avatar_dto"
        }
        filter_field = "user_id" if avtar_type in ["user-avatar", "header-avatar"] else "bot_id"
        image = get_one(avatar_collection.get(avtar_type), filter_field, bot_id)
        if image:
            img_object = image.get("avatar")
            img_object = get_one("images.chunks", "files_id", img_object)["data"]
            in_memory_file = io.BytesIO(img_object)
            return send_file(in_memory_file, mimetype='image')
        else:
            return jsonify(message="Image not found"), 404
    except Exception as e:
        logger.error("{} Not found for {}: {}".format(avtar_type, bot_id, str(e)))
        return jsonify(status='Failure', msg='Avatar Not Found'), 404

if __name__ == '__main__':
    try:
        app_host = os.environ.get('APP_HOST') or 'localhost'
        app_port = os.environ.get('APP_PORT') or '5003'
        appEvent = socketio.run(app, host=str(app_host), port=int(app_port))
        # eventlet.wsgi.server(eventlet.listen((app_host, app_port)), app)
    except Exception as e:
        logger.error(e)
