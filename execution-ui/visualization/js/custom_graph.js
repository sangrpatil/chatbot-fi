google.charts.load('visualization', { packages: ['corechart', 'line', 'bar'] });

function drawChart(graph_json) {
    let graph_type;
    let chart;
    switch (graph_json['chart_type']) {
        case 'basic_bar':
            graph_type = "bars";
            chart = new google.visualization.ColumnChart(document.getElementById('graph_container'));
            break;
        case 'combo':
            graph_type = 'combo';
            chart = new google.visualization.ComboChart(document.getElementById('graph_container'));
            break;
        default:
            graph_type = "line";
            chart = new google.visualization.LineChart(document.getElementById('graph_container'));
    }
    let data = google.visualization.arrayToDataTable(graph_json['chart_data']);
    let options = {
        title: graph_json['Chart_title'],
        vAxis: { title: graph_json['y_axis_title'] },
        hAxis: { title: graph_json['x_axis_title'] },
        seriesType: graph_type,
        fontName: 'Arial',
        colors: ['rgb(134,188,37)', 'rgb(117,120,123)', 'rgb(0,118,128)', 'rgb(98,181,229)', 'rgb(79,129,189)'],
        legend: { position: 'top', alignment: 'start', maxLines: 5}
    };
    if(graph_type == 'combo'){
        options['seriesType'] = 'bars';
        options['series'] = {};
        let series_array = graph_json['series_type']
        for (let i=0; i < series_array.length; i++){
            if(series_array[i] == 'line'){
                options['series'][i] = {type: series_array[i], targetAxisIndex: 1}
            }
            else options['series'][i] = {type: series_array[i], targetAxisIndex: 0}
        }
    }
    chart.draw(data, options);
}

var showChart = function(graph_json_param){
    google.charts.setOnLoadCallback(function(){
        drawChart(graph_json_param);
    });
}
