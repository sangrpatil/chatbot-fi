const extIsMobile = function () {
  if (typeof MobileDetect == 'function') {
    var md = new MobileDetect(window.navigator.userAgent);
    if (md && md.mobile()) {
      return true;
    }
    else return false;
  }
  else return false;
};
const getBotConfig = function () {
  const Config = {
    
    "url": "http://10.145.62.177:7005", //socket IO server url
	  "oAuthCallbackURL" : "", //SSO call back url
    "botID": "5e46703f0a4f0743942bc13e", //bot ID paramter to be sent to the server
    "getMessage": "interaction_message", // socket io bot msg emit function name
    "botHeaderText": "", //bot header label text

    "stickyLaunchImg": 'images/avatar.png', // sticky launch icon avatar url
    "botAvatar": "images/avatar.png", //bot avatar image url
    "minimizeIcon" : "", // minimize icon image url
    "maximizeIcon" : "", // maximize icon image url
    "refreshIcon" : "", //header refresh icon image url
    "closeRestartIcon" : "", //  close and restart icon image url
    "enlargeIconImg": "", // enlarge icon image url
    "shrinkIconImg": "", // shrink icon image url
    "closeIcon" : "", //header close icon image url
    "userAvatar": "", //user avatar image url

    "minimizeTitle": "Minimize", // minimize icon tooltip text
    "maximizeTitle": "Maximize", // maximize icon tooltip text
    "refreshTitle": "Refresh", //header refresh icon tooltip text
    "closeRestartTitle": "Close and Restart", //  close and restart icon tooltip text
    "closeTitle": "Close", //header close icon tooltip text
    "helpTitle": "Help", //header help icon tooltip text
    "enlargeTitle": "Enlarge", //  enlarge icon tooltip text
    "shrinkTitle": "Shrink", // shrink icon tooltip text
    "restartMessageText": "restart", //header refresh icon text message to be sent to server
    "helpMessageText": "Help", //header help icon text message to be sent to server
    "helpIcon" : "", //header help icon image url
    "sendStatePickerMsgTxt": "Enviar", // send button text message to be sent to server
    "footerCustomMessage": "", // footer message used when using bot as a standlaone app
    "tooltipText": "Record", //ASR/TTS icon tooltip text
    "inputHeaderCustomMessage": "Enter Question Below:", //placeholder for textbox

    "showchatToggle": true, //true in case user wants to have the toggle option to open and close the bot
    "isSticky": true, // true in case user wants to open chatbot with sticky icon on the right bottom
    "launchByHyperlink": false, // true in case user wants to open chatbot with hyperlink mode (as a link)
    "allowUserInput": true, //freeform textbox toggle option
    "botVoiceEnabled": false, //toggle option for disabling speech to text functionality
    "showAllText": false, //see more button functional behaviour for single click to show all the hidden text
    "lockScrollToUserMsg": true,
    "browserFullScreenFlag": true, //toggle to enable/disable the opening of rich media in full screen
    "linkInSameTab": true,  // toggle to enable/disable the opening of a link in the same tab window.
    "openLocInNewWin": true, // toggle to open the location urls in the new window
    "movableChatbot": false && !extIsMobile(), // toggle to enable/disbale movable bot window functionality
    "isResizeable": false && !extIsMobile(), // toggle to enable/disable the resizeable bot window functionality
    "minimizeable": false, // toggle to enable/disable the bot minimizeable dock functionality
    "showCloseRestartIcon": false, //toggle to enable/disable the close and restart functionlity for close icon 
    "enlargeOption": false && !extIsMobile(), // toggle to enable/disbale the enlarge bot window option
    
    "messageFontSizeInPx": "10pt !important", // bot and user message font size
    "botWidth": "100%", // bot width in px or vw
    "botHeight": "100%", //bot height in px or vh
    "botHeaderColor": "rgb(247, 247, 247)", // bot header color value
    "botHeaderHeight": "45px", //bot header height in px
    "botBackground": "rgb(97, 99, 101)", //bot window background color
    "botConverseBackground": "#000", //bot message bubble color
    "botConverseColor": "#fff", //bot message text color
    "userConverseBackground": "#fff", //user message bubble color
    "userConverseColor": "#000", // user message text color
    "btnActive": "rgb(200, 240, 250) !important", //color for ASR icon when it is active
    "micActive": "rgb(200, 240, 250) !important", //color for ASR icon when mic is active
    "micNormal": "#313232", //color for mic when it is inactive
    "enlargedHeight": "90vh", // bot enlarged height in px or vh
    "enlargedWidth": "97vw", // bot enlarged width in px or vw
    
    "inputMaxRows": 1, //no. of lines for textbox input
    "botMsgTimeOut": 1000, //time delay for bot messages
    "subBotMsgTimeout": 1000, //time delay for sub bot messages
    "noResponseTimeout": 1800000000, //inactivity time limit for the bot
    "noResponseTimeoutMsg": "Bot Timedout.", //bot message to be shown in case bot becomes inactive after inactivity time lmit
    "buttonsVisibleCount": 18, //no. of buttons to be shown at a time
    "toggleText": "see more", //text for the see more otpion label
    "msgMaxHeightToggle": 400,
    "oAuthTokenCookieName" : "Auth",

    "isMobile": function () {
      if (typeof MobileDetect == 'function') {
        var md = new MobileDetect(window.navigator.userAgent);
        if (md && md.mobile()) {
          return true;
        }
        else return false;
      }
      else return false;
    },
    "isIE": function () {
      ua = navigator.userAgent;
      /* MSIE used to detect old browsers and Trident used to newer ones*/
      var is_ie = ua.indexOf("MSIE ") > -1 || ua.indexOf("Trident/") > -1;
      return is_ie;
    }
  }
  return Config;
}
/* Toogle nav menu in mobile */
  $('.mobile-toggle-nav').click(function() {
  $(this).toggleClass('is-active');
  $('.app-container').toggleClass('sidebar-mobile-open');
});